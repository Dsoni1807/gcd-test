import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AssetEvent from './asset-event';
import AssetEventDetail from './asset-event-detail';
import AssetEventUpdate from './asset-event-update';
import AssetEventDeleteDialog from './asset-event-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={AssetEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:assetId/new`} component={AssetEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={AssetEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/:assetId/edit`} component={AssetEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AssetEventDetail} />
      <ErrorBoundaryRoute path={match.url} component={AssetEvent} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={AssetEventDeleteDialog} />
  </>
);

export default Routes;
