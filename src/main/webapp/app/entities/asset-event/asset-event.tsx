import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './asset-event.reducer';
import { IAssetEvent } from 'app/shared/model/asset-event.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAssetEventProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const AssetEvent = (props: IAssetEventProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { assetEventList, match, loading } = props;
  return (
    <div>
      <h2 id="asset-event-heading">
        Asset Events
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Asset Event
        </Link>
      </h2>
      <div className="table-responsive">
        {assetEventList && assetEventList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Status Id</th>
                <th>Check Out To</th>
                <th>Assigned To</th>
                <th>Check Out Date</th>
                <th>Check In From</th>
                <th>Returned From</th>
                <th>Check In Date</th>
                <th>Lease Start Date</th>
                <th>Lease From Customer Id</th>
                <th>Lease End Date</th>
                <th>Lease Return Date</th>
                <th>Lost Date</th>
                <th>Found Date</th>
                <th>Repair Schedule Date</th>
                <th>Repair Assigned To</th>
                <th>Repair Completed Date</th>
                <th>Repair Cost</th>
                <th>Broken Date</th>
                <th>Dispose Date</th>
                <th>Dispose To</th>
                <th>Donate Date</th>
                <th>Donate To</th>
                <th>Donate Value</th>
                <th>Deductible</th>
                <th>Sell Date</th>
                <th>Sell To</th>
                <th>Sell Value</th>
                <th>Is Due Date</th>
                <th>Due Date</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th>Asset</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetEventList.map((assetEvent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${assetEvent.id}`} color="link" size="sm">
                      {assetEvent.id}
                    </Button>
                  </td>
                  <td>{assetEvent.statusId}</td>
                  <td>{assetEvent.checkOutTo}</td>
                  <td>{assetEvent.assignedTo}</td>
                  <td>
                    {assetEvent.checkOutDate ? (
                      <TextFormat type="date" value={assetEvent.checkOutDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.checkInFrom}</td>
                  <td>{assetEvent.returnedFrom}</td>
                  <td>
                    {assetEvent.checkInDate ? (
                      <TextFormat type="date" value={assetEvent.checkInDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseStartDate ? (
                      <TextFormat type="date" value={assetEvent.leaseStartDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.leaseFromCustomerId}</td>
                  <td>
                    {assetEvent.leaseEndDate ? (
                      <TextFormat type="date" value={assetEvent.leaseEndDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseReturnDate ? (
                      <TextFormat type="date" value={assetEvent.leaseReturnDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.lostDate ? <TextFormat type="date" value={assetEvent.lostDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.foundDate ? <TextFormat type="date" value={assetEvent.foundDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.repairScheduleDate ? (
                      <TextFormat type="date" value={assetEvent.repairScheduleDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairAssignedTo}</td>
                  <td>
                    {assetEvent.repairCompletedDate ? (
                      <TextFormat type="date" value={assetEvent.repairCompletedDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairCost}</td>
                  <td>
                    {assetEvent.brokenDate ? <TextFormat type="date" value={assetEvent.brokenDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.disposeDate ? (
                      <TextFormat type="date" value={assetEvent.disposeDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.disposeTo}</td>
                  <td>
                    {assetEvent.donateDate ? <TextFormat type="date" value={assetEvent.donateDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.donateTo}</td>
                  <td>{assetEvent.donateValue}</td>
                  <td>{assetEvent.deductible ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.sellDate ? <TextFormat type="date" value={assetEvent.sellDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.sellTo}</td>
                  <td>{assetEvent.sellValue}</td>
                  <td>{assetEvent.isDueDate ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.dueDate ? <TextFormat type="date" value={assetEvent.dueDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.siteId}</td>
                  <td>{assetEvent.locationId}</td>
                  <td>{assetEvent.comments}</td>
                  <td>{assetEvent.businessId}</td>
                  <td>{assetEvent.assetId ? <Link to={`asset/${assetEvent.assetId}`}>{assetEvent.assetId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${assetEvent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${assetEvent.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${assetEvent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Asset Events found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ assetEvent }: IRootState) => ({
  assetEventList: assetEvent.entities,
  loading: assetEvent.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AssetEvent);
