import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IAsset } from 'app/shared/model/asset.model';
import { getEntities as getAssets } from 'app/entities/asset/asset.reducer';
import { getEntity, updateEntity, createEntity, reset } from './asset-event.reducer';
import { IAssetEvent } from 'app/shared/model/asset-event.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAssetEventUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string, assetId: string }> {}

export const AssetEventUpdate = (props: IAssetEventUpdateProps) => {
  

  const [assetId, setAssetId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
  const [isAssetNew, setIsAssetNew] = useState(props.match.params && props.match.params.assetId);

  const { assetEventEntity, assets, loading, updating } = props;

  const handleClose = () => {
    props.history.goBack();
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getAssets();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...assetEventEntity,
        ...values,
      };

      if(isAssetNew){
        entity.assetId = props.match.params.assetId;
      }

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gcdApp.assetEvent.home.createOrEditLabel">Create or edit a AssetEvent</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : assetEventEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="asset-event-id">ID</Label>
                  <AvInput id="asset-event-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="statusIdLabel" for="asset-event-statusId">
                  Status Id
                </Label>
                <AvField id="asset-event-statusId" type="string" className="form-control" name="statusId" />
              </AvGroup>
              <AvGroup>
                <Label id="checkOutToLabel" for="asset-event-checkOutTo">
                  Check Out To
                </Label>
                <AvField id="asset-event-checkOutTo" type="string" className="form-control" name="checkOutTo" />
              </AvGroup>
              <AvGroup>
                <Label id="assignedToLabel" for="asset-event-assignedTo">
                  Assigned To
                </Label>
                <AvField id="asset-event-assignedTo" type="string" className="form-control" name="assignedTo" />
              </AvGroup>
              <AvGroup>
                <Label id="checkOutDateLabel" for="asset-event-checkOutDate">
                  Check Out Date
                </Label>
                <AvField id="asset-event-checkOutDate" type="date" className="form-control" name="checkOutDate" />
              </AvGroup>
              <AvGroup>
                <Label id="checkInFromLabel" for="asset-event-checkInFrom">
                  Check In From
                </Label>
                <AvField id="asset-event-checkInFrom" type="string" className="form-control" name="checkInFrom" />
              </AvGroup>
              <AvGroup>
                <Label id="returnedFromLabel" for="asset-event-returnedFrom">
                  Returned From
                </Label>
                <AvField id="asset-event-returnedFrom" type="string" className="form-control" name="returnedFrom" />
              </AvGroup>
              <AvGroup>
                <Label id="checkInDateLabel" for="asset-event-checkInDate">
                  Check In Date
                </Label>
                <AvField id="asset-event-checkInDate" type="date" className="form-control" name="checkInDate" />
              </AvGroup>
              <AvGroup>
                <Label id="leaseStartDateLabel" for="asset-event-leaseStartDate">
                  Lease Start Date
                </Label>
                <AvField id="asset-event-leaseStartDate" type="date" className="form-control" name="leaseStartDate" />
              </AvGroup>
              <AvGroup>
                <Label id="leaseFromCustomerIdLabel" for="asset-event-leaseFromCustomerId">
                  Lease From Customer Id
                </Label>
                <AvField id="asset-event-leaseFromCustomerId" type="string" className="form-control" name="leaseFromCustomerId" />
              </AvGroup>
              <AvGroup>
                <Label id="leaseEndDateLabel" for="asset-event-leaseEndDate">
                  Lease End Date
                </Label>
                <AvField id="asset-event-leaseEndDate" type="date" className="form-control" name="leaseEndDate" />
              </AvGroup>
              <AvGroup>
                <Label id="leaseReturnDateLabel" for="asset-event-leaseReturnDate">
                  Lease Return Date
                </Label>
                <AvField id="asset-event-leaseReturnDate" type="date" className="form-control" name="leaseReturnDate" />
              </AvGroup>
              <AvGroup>
                <Label id="lostDateLabel" for="asset-event-lostDate">
                  Lost Date
                </Label>
                <AvField id="asset-event-lostDate" type="date" className="form-control" name="lostDate" />
              </AvGroup>
              <AvGroup>
                <Label id="foundDateLabel" for="asset-event-foundDate">
                  Found Date
                </Label>
                <AvField id="asset-event-foundDate" type="date" className="form-control" name="foundDate" />
              </AvGroup>
              <AvGroup>
                <Label id="repairScheduleDateLabel" for="asset-event-repairScheduleDate">
                  Repair Schedule Date
                </Label>
                <AvField id="asset-event-repairScheduleDate" type="date" className="form-control" name="repairScheduleDate" />
              </AvGroup>
              <AvGroup>
                <Label id="repairAssignedToLabel" for="asset-event-repairAssignedTo">
                  Repair Assigned To
                </Label>
                <AvField
                  id="asset-event-repairAssignedTo"
                  type="text"
                  name="repairAssignedTo"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="repairCompletedDateLabel" for="asset-event-repairCompletedDate">
                  Repair Completed Date
                </Label>
                <AvField id="asset-event-repairCompletedDate" type="date" className="form-control" name="repairCompletedDate" />
              </AvGroup>
              <AvGroup>
                <Label id="repairCostLabel" for="asset-event-repairCost">
                  Repair Cost
                </Label>
                <AvField id="asset-event-repairCost" type="text" name="repairCost" />
              </AvGroup>
              <AvGroup>
                <Label id="brokenDateLabel" for="asset-event-brokenDate">
                  Broken Date
                </Label>
                <AvField id="asset-event-brokenDate" type="date" className="form-control" name="brokenDate" />
              </AvGroup>
              <AvGroup>
                <Label id="disposeDateLabel" for="asset-event-disposeDate">
                  Dispose Date
                </Label>
                <AvField id="asset-event-disposeDate" type="date" className="form-control" name="disposeDate" />
              </AvGroup>
              <AvGroup>
                <Label id="disposeToLabel" for="asset-event-disposeTo">
                  Dispose To
                </Label>
                <AvField id="asset-event-disposeTo" type="text" name="disposeTo" />
              </AvGroup>
              <AvGroup>
                <Label id="donateDateLabel" for="asset-event-donateDate">
                  Donate Date
                </Label>
                <AvField id="asset-event-donateDate" type="date" className="form-control" name="donateDate" />
              </AvGroup>
              <AvGroup>
                <Label id="donateToLabel" for="asset-event-donateTo">
                  Donate To
                </Label>
                <AvField
                  id="asset-event-donateTo"
                  type="text"
                  name="donateTo"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="donateValueLabel" for="asset-event-donateValue">
                  Donate Value
                </Label>
                <AvField id="asset-event-donateValue" type="text" name="donateValue" />
              </AvGroup>
              <AvGroup check>
                <Label id="deductibleLabel">
                  <AvInput id="asset-event-deductible" type="checkbox" className="form-check-input" name="deductible" />
                  Deductible
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="sellDateLabel" for="asset-event-sellDate">
                  Sell Date
                </Label>
                <AvField id="asset-event-sellDate" type="date" className="form-control" name="sellDate" />
              </AvGroup>
              <AvGroup>
                <Label id="sellToLabel" for="asset-event-sellTo">
                  Sell To
                </Label>
                <AvField
                  id="asset-event-sellTo"
                  type="text"
                  name="sellTo"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="sellValueLabel" for="asset-event-sellValue">
                  Sell Value
                </Label>
                <AvField id="asset-event-sellValue" type="text" name="sellValue" />
              </AvGroup>
              <AvGroup check>
                <Label id="isDueDateLabel">
                  <AvInput id="asset-event-isDueDate" type="checkbox" className="form-check-input" name="isDueDate" />
                  Is Due Date
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="dueDateLabel" for="asset-event-dueDate">
                  Due Date
                </Label>
                <AvField id="asset-event-dueDate" type="date" className="form-control" name="dueDate" />
              </AvGroup>
              <AvGroup>
                <Label id="siteIdLabel" for="asset-event-siteId">
                  Site Id
                </Label>
                <AvField id="asset-event-siteId" type="string" className="form-control" name="siteId" />
              </AvGroup>
              <AvGroup>
                <Label id="locationIdLabel" for="asset-event-locationId">
                  Location Id
                </Label>
                <AvField id="asset-event-locationId" type="string" className="form-control" name="locationId" />
              </AvGroup>
              <AvGroup>
                <Label id="commentsLabel" for="asset-event-comments">
                  Comments
                </Label>
                <AvField id="asset-event-comments" type="text" name="comments" />
              </AvGroup>
              <AvGroup>
                <Label id="businessIdLabel" for="asset-event-businessId">
                  Business Id
                </Label>
                <AvField id="asset-event-businessId" type="string" className="form-control" name="businessId" />
              </AvGroup>
              <AvGroup className={isAssetNew ? "d-none" : ""}>
                <Label for="asset-event-asset">Asset</Label>
                <AvInput id="asset-event-asset" type="select" className="form-control" name="assetId">
                  <option value="" key="0" />
                  {assets
                    ? assets.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button onClick={() => props.history.goBack()} id="cancel-save" color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  assets: storeState.asset.entities,
  assetEventEntity: storeState.assetEvent.entity,
  loading: storeState.assetEvent.loading,
  updating: storeState.assetEvent.updating,
  updateSuccess: storeState.assetEvent.updateSuccess,
});

const mapDispatchToProps = {
  getAssets,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AssetEventUpdate);
