import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './asset-event.reducer';
import { IAssetEvent } from 'app/shared/model/asset-event.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAssetEventDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AssetEventDetail = (props: IAssetEventDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { assetEventEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          AssetEvent [<b>{assetEventEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="statusId">Status Id</span>
          </dt>
          <dd>{assetEventEntity.statusId}</dd>
          <dt>
            <span id="checkOutTo">Check Out To</span>
          </dt>
          <dd>{assetEventEntity.checkOutTo}</dd>
          <dt>
            <span id="assignedTo">Assigned To</span>
          </dt>
          <dd>{assetEventEntity.assignedTo}</dd>
          <dt>
            <span id="checkOutDate">Check Out Date</span>
          </dt>
          <dd>
            {assetEventEntity.checkOutDate ? (
              <TextFormat value={assetEventEntity.checkOutDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="checkInFrom">Check In From</span>
          </dt>
          <dd>{assetEventEntity.checkInFrom}</dd>
          <dt>
            <span id="returnedFrom">Returned From</span>
          </dt>
          <dd>{assetEventEntity.returnedFrom}</dd>
          <dt>
            <span id="checkInDate">Check In Date</span>
          </dt>
          <dd>
            {assetEventEntity.checkInDate ? (
              <TextFormat value={assetEventEntity.checkInDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="leaseStartDate">Lease Start Date</span>
          </dt>
          <dd>
            {assetEventEntity.leaseStartDate ? (
              <TextFormat value={assetEventEntity.leaseStartDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="leaseFromCustomerId">Lease From Customer Id</span>
          </dt>
          <dd>{assetEventEntity.leaseFromCustomerId}</dd>
          <dt>
            <span id="leaseEndDate">Lease End Date</span>
          </dt>
          <dd>
            {assetEventEntity.leaseEndDate ? (
              <TextFormat value={assetEventEntity.leaseEndDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="leaseReturnDate">Lease Return Date</span>
          </dt>
          <dd>
            {assetEventEntity.leaseReturnDate ? (
              <TextFormat value={assetEventEntity.leaseReturnDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="lostDate">Lost Date</span>
          </dt>
          <dd>
            {assetEventEntity.lostDate ? <TextFormat value={assetEventEntity.lostDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="foundDate">Found Date</span>
          </dt>
          <dd>
            {assetEventEntity.foundDate ? (
              <TextFormat value={assetEventEntity.foundDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="repairScheduleDate">Repair Schedule Date</span>
          </dt>
          <dd>
            {assetEventEntity.repairScheduleDate ? (
              <TextFormat value={assetEventEntity.repairScheduleDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="repairAssignedTo">Repair Assigned To</span>
          </dt>
          <dd>{assetEventEntity.repairAssignedTo}</dd>
          <dt>
            <span id="repairCompletedDate">Repair Completed Date</span>
          </dt>
          <dd>
            {assetEventEntity.repairCompletedDate ? (
              <TextFormat value={assetEventEntity.repairCompletedDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="repairCost">Repair Cost</span>
          </dt>
          <dd>{assetEventEntity.repairCost}</dd>
          <dt>
            <span id="brokenDate">Broken Date</span>
          </dt>
          <dd>
            {assetEventEntity.brokenDate ? (
              <TextFormat value={assetEventEntity.brokenDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="disposeDate">Dispose Date</span>
          </dt>
          <dd>
            {assetEventEntity.disposeDate ? (
              <TextFormat value={assetEventEntity.disposeDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="disposeTo">Dispose To</span>
          </dt>
          <dd>{assetEventEntity.disposeTo}</dd>
          <dt>
            <span id="donateDate">Donate Date</span>
          </dt>
          <dd>
            {assetEventEntity.donateDate ? (
              <TextFormat value={assetEventEntity.donateDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="donateTo">Donate To</span>
          </dt>
          <dd>{assetEventEntity.donateTo}</dd>
          <dt>
            <span id="donateValue">Donate Value</span>
          </dt>
          <dd>{assetEventEntity.donateValue}</dd>
          <dt>
            <span id="deductible">Deductible</span>
          </dt>
          <dd>{assetEventEntity.deductible ? 'true' : 'false'}</dd>
          <dt>
            <span id="sellDate">Sell Date</span>
          </dt>
          <dd>
            {assetEventEntity.sellDate ? <TextFormat value={assetEventEntity.sellDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="sellTo">Sell To</span>
          </dt>
          <dd>{assetEventEntity.sellTo}</dd>
          <dt>
            <span id="sellValue">Sell Value</span>
          </dt>
          <dd>{assetEventEntity.sellValue}</dd>
          <dt>
            <span id="isDueDate">Is Due Date</span>
          </dt>
          <dd>{assetEventEntity.isDueDate ? 'true' : 'false'}</dd>
          <dt>
            <span id="dueDate">Due Date</span>
          </dt>
          <dd>
            {assetEventEntity.dueDate ? <TextFormat value={assetEventEntity.dueDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="siteId">Site Id</span>
          </dt>
          <dd>{assetEventEntity.siteId}</dd>
          <dt>
            <span id="locationId">Location Id</span>
          </dt>
          <dd>{assetEventEntity.locationId}</dd>
          <dt>
            <span id="comments">Comments</span>
          </dt>
          <dd>{assetEventEntity.comments}</dd>
          <dt>
            <span id="businessId">Business Id</span>
          </dt>
          <dd>{assetEventEntity.businessId}</dd>
          <dt>Asset</dt>
          <dd>{assetEventEntity.assetId ? assetEventEntity.assetId : ''}</dd>
        </dl>
        <Button onClick={() => props.history.goBack()} replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/asset-event/${assetEventEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ assetEvent }: IRootState) => ({
  assetEventEntity: assetEvent.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AssetEventDetail);
