import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IAssetEvent, defaultValue } from 'app/shared/model/asset-event.model';

export const ACTION_TYPES = {
  FETCH_ASSETEVENT_LIST: 'assetEvent/FETCH_ASSETEVENT_LIST',
  FETCH_ASSETEVENT: 'assetEvent/FETCH_ASSETEVENT',
  CREATE_ASSETEVENT: 'assetEvent/CREATE_ASSETEVENT',
  UPDATE_ASSETEVENT: 'assetEvent/UPDATE_ASSETEVENT',
  DELETE_ASSETEVENT: 'assetEvent/DELETE_ASSETEVENT',
  RESET: 'assetEvent/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAssetEvent>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type AssetEventState = Readonly<typeof initialState>;

// Reducer

export default (state: AssetEventState = initialState, action): AssetEventState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ASSETEVENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ASSETEVENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_ASSETEVENT):
    case REQUEST(ACTION_TYPES.UPDATE_ASSETEVENT):
    case REQUEST(ACTION_TYPES.DELETE_ASSETEVENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_ASSETEVENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ASSETEVENT):
    case FAILURE(ACTION_TYPES.CREATE_ASSETEVENT):
    case FAILURE(ACTION_TYPES.UPDATE_ASSETEVENT):
    case FAILURE(ACTION_TYPES.DELETE_ASSETEVENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ASSETEVENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ASSETEVENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_ASSETEVENT):
    case SUCCESS(ACTION_TYPES.UPDATE_ASSETEVENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_ASSETEVENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/asset-events';

// Actions

export const getEntities: ICrudGetAllAction<IAssetEvent> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_ASSETEVENT_LIST,
  payload: axios.get<IAssetEvent>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IAssetEvent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ASSETEVENT,
    payload: axios.get<IAssetEvent>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IAssetEvent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ASSETEVENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAssetEvent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ASSETEVENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAssetEvent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ASSETEVENT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
