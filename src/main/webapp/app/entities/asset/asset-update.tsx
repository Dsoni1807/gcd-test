import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './asset.reducer';
import { IAsset } from 'app/shared/model/asset.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { Table } from 'reactstrap';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

export interface IAssetUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AssetUpdate = (props: IAssetUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { assetEntity, loading, updating } = props;

  const { photo, photoContentType } = assetEntity;

  const handleClose = () => {
    props.history.push('/asset');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...assetEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gcdApp.asset.home.createOrEditLabel">Create or edit a Asset</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : assetEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="asset-id">ID</Label>
                  <AvInput id="asset-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="asset-name">
                  Name
                </Label>
                <AvField
                  id="asset-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="tagLabel" for="asset-tag">
                  Tag
                </Label>
                <AvField
                  id="asset-tag"
                  type="text"
                  name="tag"
                  validate={{
                    maxLength: { value: 150, errorMessage: 'This field cannot be longer than 150 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="customTagLabel" for="asset-customTag">
                  Custom Tag
                </Label>
                <AvField
                  id="asset-customTag"
                  type="text"
                  name="customTag"
                  validate={{
                    maxLength: { value: 150, errorMessage: 'This field cannot be longer than 150 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="asset-description">
                  Description
                </Label>
                <AvField
                  id="asset-description"
                  type="text"
                  name="description"
                  validate={{
                    maxLength: { value: 500, errorMessage: 'This field cannot be longer than 500 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="statusIdLabel" for="asset-statusId">
                  Status Id
                </Label>
                <AvField
                  id="asset-statusId"
                  type="string"
                  className="form-control"
                  name="statusId"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="purchaseDateLabel" for="asset-purchaseDate">
                  Purchase Date
                </Label>
                <AvField
                  id="asset-purchaseDate"
                  type="date"
                  className="form-control"
                  name="purchaseDate"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="purchaseFromLabel" for="asset-purchaseFrom">
                  Purchase From
                </Label>
                <AvField
                  id="asset-purchaseFrom"
                  type="text"
                  name="purchaseFrom"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="purchaseCostLabel" for="asset-purchaseCost">
                  Purchase Cost
                </Label>
                <AvField id="asset-purchaseCost" type="text" name="purchaseCost" />
              </AvGroup>
              <AvGroup>
                <Label id="brandLabel" for="asset-brand">
                  Brand
                </Label>
                <AvField
                  id="asset-brand"
                  type="text"
                  name="brand"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="modelLabel" for="asset-model">
                  Model
                </Label>
                <AvField
                  id="asset-model"
                  type="text"
                  name="model"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="serialNoLabel" for="asset-serialNo">
                  Serial No
                </Label>
                <AvField
                  id="asset-serialNo"
                  type="text"
                  name="serialNo"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="siteIdLabel" for="asset-siteId">
                  Site Id
                </Label>
                <AvField id="asset-siteId" type="string" className="form-control" name="siteId" />
              </AvGroup>
              <AvGroup>
                <Label id="locationIdLabel" for="asset-locationId">
                  Location Id
                </Label>
                <AvField id="asset-locationId" type="string" className="form-control" name="locationId" />
              </AvGroup>
              <AvGroup>
                <Label id="departmentIdLabel" for="asset-departmentId">
                  Department Id
                </Label>
                <AvField id="asset-departmentId" type="string" className="form-control" name="departmentId" />
              </AvGroup>
              <AvGroup>
                <Label id="categoryIdLabel" for="asset-categoryId">
                  Category Id
                </Label>
                <AvField id="asset-categoryId" type="string" className="form-control" name="categoryId" />
              </AvGroup>
              <AvGroup>
                <AvGroup>
                  <Label id="photoLabel" for="photo">
                    Photo
                  </Label>
                  <br />
                  {photo ? (
                    <div>
                      {photoContentType ? (
                        <a onClick={openFile(photoContentType, photo)}>
                          <img src={`data:${photoContentType};base64,${photo}`} style={{ maxHeight: '100px' }} />
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {photoContentType}, {byteSize(photo)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('photo')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_photo" type="file" onChange={onBlobChange(true, 'photo')} accept="image/*" />
                  <AvInput type="hidden" name="photo" value={photo} />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="eolDateLabel" for="asset-eolDate">
                  Eol Date
                </Label>
                <AvField id="asset-eolDate" type="date" className="form-control" name="eolDate" />
              </AvGroup>
              <AvGroup>
                <Label id="assetTypeIdLabel" for="asset-assetTypeId">
                  Asset Type Id
                </Label>
                <AvField id="asset-assetTypeId" type="string" className="form-control" name="assetTypeId" />
              </AvGroup>
              <AvGroup>
                <Label id="systemNameLabel" for="asset-systemName">
                  System Name
                </Label>
                <AvField
                  id="asset-systemName"
                  type="text"
                  name="systemName"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="processorLabel" for="asset-processor">
                  Processor
                </Label>
                <AvField
                  id="asset-processor"
                  type="text"
                  name="processor"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="hardDriveLabel" for="asset-hardDrive">
                  Hard Drive
                </Label>
                <AvField
                  id="asset-hardDrive"
                  type="text"
                  name="hardDrive"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="memoryLabel" for="asset-memory">
                  Memory
                </Label>
                <AvField
                  id="asset-memory"
                  type="text"
                  name="memory"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="operatingSystemLabel" for="asset-operatingSystem">
                  Operating System
                </Label>
                <AvField
                  id="asset-operatingSystem"
                  type="text"
                  name="operatingSystem"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="workGroupLabel" for="asset-workGroup">
                  Work Group
                </Label>
                <AvField
                  id="asset-workGroup"
                  type="text"
                  name="workGroup"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="ipAddressLabel" for="asset-ipAddress">
                  Ip Address
                </Label>
                <AvField
                  id="asset-ipAddress"
                  type="text"
                  name="ipAddress"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="biosLabel" for="asset-bios">
                  Bios
                </Label>
                <AvField
                  id="asset-bios"
                  type="text"
                  name="bios"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="firmwareLabel" for="asset-firmware">
                  Firmware
                </Label>
                <AvField
                  id="asset-firmware"
                  type="text"
                  name="firmware"
                  validate={{
                    maxLength: { value: 250, errorMessage: 'This field cannot be longer than 250 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="commentsLabel" for="asset-comments">
                  Comments
                </Label>
                <AvField id="asset-comments" type="text" name="comments" />
              </AvGroup>
              <AvGroup>
                <Label id="businessIdLabel" for="asset-businessId">
                  Business Id
                </Label>
                <AvField id="asset-businessId" type="string" className="form-control" name="businessId" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/asset" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
      {!isNew && (
      <Row className="justify-content-center">
        <Col md="12">
        <Tabs>
          <TabList>
            <Tab>Check Out</Tab>
            <Tab>Check In</Tab>
          </TabList>
          <TabPanel>
          <div className="table-responsive">
        {assetEntity.checkOuts && assetEntity.checkOuts.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Status Id</th>
                <th>Check Out To</th>
                <th>Assigned To</th>
                <th>Check Out Date</th>
                <th>Check In From</th>
                <th>Returned From</th>
                <th>Check In Date</th>
                <th>Lease Start Date</th>
                <th>Lease From Customer Id</th>
                <th>Lease End Date</th>
                <th>Lease Return Date</th>
                <th>Lost Date</th>
                <th>Found Date</th>
                <th>Repair Schedule Date</th>
                <th>Repair Assigned To</th>
                <th>Repair Completed Date</th>
                <th>Repair Cost</th>
                <th>Broken Date</th>
                <th>Dispose Date</th>
                <th>Dispose To</th>
                <th>Donate Date</th>
                <th>Donate To</th>
                <th>Donate Value</th>
                <th>Deductible</th>
                <th>Sell Date</th>
                <th>Sell To</th>
                <th>Sell Value</th>
                <th>Is Due Date</th>
                <th>Due Date</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th>Asset</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetEntity.checkOuts.map((assetEvent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="link" size="sm">
                      {assetEvent.id}
                    </Button>
                  </td>
                  <td>{assetEvent.statusId}</td>
                  <td>{assetEvent.checkOutTo}</td>
                  <td>{assetEvent.assignedTo}</td>
                  <td>
                    {assetEvent.checkOutDate ? (
                      <TextFormat type="date" value={assetEvent.checkOutDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.checkInFrom}</td>
                  <td>{assetEvent.returnedFrom}</td>
                  <td>
                    {assetEvent.checkInDate ? (
                      <TextFormat type="date" value={assetEvent.checkInDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseStartDate ? (
                      <TextFormat type="date" value={assetEvent.leaseStartDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.leaseFromCustomerId}</td>
                  <td>
                    {assetEvent.leaseEndDate ? (
                      <TextFormat type="date" value={assetEvent.leaseEndDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseReturnDate ? (
                      <TextFormat type="date" value={assetEvent.leaseReturnDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.lostDate ? <TextFormat type="date" value={assetEvent.lostDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.foundDate ? <TextFormat type="date" value={assetEvent.foundDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.repairScheduleDate ? (
                      <TextFormat type="date" value={assetEvent.repairScheduleDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairAssignedTo}</td>
                  <td>
                    {assetEvent.repairCompletedDate ? (
                      <TextFormat type="date" value={assetEvent.repairCompletedDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairCost}</td>
                  <td>
                    {assetEvent.brokenDate ? <TextFormat type="date" value={assetEvent.brokenDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.disposeDate ? (
                      <TextFormat type="date" value={assetEvent.disposeDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.disposeTo}</td>
                  <td>
                    {assetEvent.donateDate ? <TextFormat type="date" value={assetEvent.donateDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.donateTo}</td>
                  <td>{assetEvent.donateValue}</td>
                  <td>{assetEvent.deductible ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.sellDate ? <TextFormat type="date" value={assetEvent.sellDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.sellTo}</td>
                  <td>{assetEvent.sellValue}</td>
                  <td>{assetEvent.isDueDate ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.dueDate ? <TextFormat type="date" value={assetEvent.dueDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.siteId}</td>
                  <td>{assetEvent.locationId}</td>
                  <td>{assetEvent.comments}</td>
                  <td>{assetEvent.businessId}</td>
                  <td>{assetEvent.assetId ? <Link to={`asset/${assetEvent.assetId}`}>{assetEvent.assetId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/${assetEvent.assetId}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Check Out Asset Events found</div>
        )}
      </div>
          </TabPanel>
          <TabPanel>
          <div className="table-responsive">
        {assetEntity.checkIns && assetEntity.checkIns.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Status Id</th>
                <th>Check Out To</th>
                <th>Assigned To</th>
                <th>Check Out Date</th>
                <th>Check In From</th>
                <th>Returned From</th>
                <th>Check In Date</th>
                <th>Lease Start Date</th>
                <th>Lease From Customer Id</th>
                <th>Lease End Date</th>
                <th>Lease Return Date</th>
                <th>Lost Date</th>
                <th>Found Date</th>
                <th>Repair Schedule Date</th>
                <th>Repair Assigned To</th>
                <th>Repair Completed Date</th>
                <th>Repair Cost</th>
                <th>Broken Date</th>
                <th>Dispose Date</th>
                <th>Dispose To</th>
                <th>Donate Date</th>
                <th>Donate To</th>
                <th>Donate Value</th>
                <th>Deductible</th>
                <th>Sell Date</th>
                <th>Sell To</th>
                <th>Sell Value</th>
                <th>Is Due Date</th>
                <th>Due Date</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th>Asset</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetEntity.checkIns.map((assetEvent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                  <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="link" size="sm">
                      {assetEvent.id}
                    </Button>
                  </td>
                  <td>{assetEvent.statusId}</td>
                  <td>{assetEvent.checkOutTo}</td>
                  <td>{assetEvent.assignedTo}</td>
                  <td>
                    {assetEvent.checkOutDate ? (
                      <TextFormat type="date" value={assetEvent.checkOutDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.checkInFrom}</td>
                  <td>{assetEvent.returnedFrom}</td>
                  <td>
                    {assetEvent.checkInDate ? (
                      <TextFormat type="date" value={assetEvent.checkInDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseStartDate ? (
                      <TextFormat type="date" value={assetEvent.leaseStartDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.leaseFromCustomerId}</td>
                  <td>
                    {assetEvent.leaseEndDate ? (
                      <TextFormat type="date" value={assetEvent.leaseEndDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseReturnDate ? (
                      <TextFormat type="date" value={assetEvent.leaseReturnDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.lostDate ? <TextFormat type="date" value={assetEvent.lostDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.foundDate ? <TextFormat type="date" value={assetEvent.foundDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.repairScheduleDate ? (
                      <TextFormat type="date" value={assetEvent.repairScheduleDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairAssignedTo}</td>
                  <td>
                    {assetEvent.repairCompletedDate ? (
                      <TextFormat type="date" value={assetEvent.repairCompletedDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairCost}</td>
                  <td>
                    {assetEvent.brokenDate ? <TextFormat type="date" value={assetEvent.brokenDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.disposeDate ? (
                      <TextFormat type="date" value={assetEvent.disposeDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.disposeTo}</td>
                  <td>
                    {assetEvent.donateDate ? <TextFormat type="date" value={assetEvent.donateDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.donateTo}</td>
                  <td>{assetEvent.donateValue}</td>
                  <td>{assetEvent.deductible ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.sellDate ? <TextFormat type="date" value={assetEvent.sellDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.sellTo}</td>
                  <td>{assetEvent.sellValue}</td>
                  <td>{assetEvent.isDueDate ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.dueDate ? <TextFormat type="date" value={assetEvent.dueDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.siteId}</td>
                  <td>{assetEvent.locationId}</td>
                  <td>{assetEvent.comments}</td>
                  <td>{assetEvent.businessId}</td>
                  <td>{assetEvent.assetId ? <Link to={`asset/${assetEvent.assetId}`}>{assetEvent.assetId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/${assetEvent.assetId}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Check in Asset Events found</div>
        )}
      </div>
          
          </TabPanel>
        </Tabs>
        
        <Link to={`/asset-event/${assetEntity.id}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Asset event
        </Link>
        </Col>
      </Row>  
      )}
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  assetEntity: storeState.asset.entity,
  loading: storeState.asset.loading,
  updating: storeState.asset.updating,
  updateSuccess: storeState.asset.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AssetUpdate);
