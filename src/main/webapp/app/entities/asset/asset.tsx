import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { openFile, byteSize, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './asset.reducer';
import { IAsset } from 'app/shared/model/asset.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAssetProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Asset = (props: IAssetProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { assetList, match, loading } = props;
  return (
    <div>
      <h2 id="asset-heading">
        Assets
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Asset
        </Link>
      </h2>
      <div className="table-responsive">
        {assetList && assetList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Tag</th>
                <th>Custom Tag</th>
                <th>Description</th>
                <th>Status Id</th>
                <th>Purchase Date</th>
                <th>Purchase From</th>
                <th>Purchase Cost</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Serial No</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Department Id</th>
                <th>Category Id</th>
                <th>Photo</th>
                <th>Eol Date</th>
                <th>Asset Type Id</th>
                <th>System Name</th>
                <th>Processor</th>
                <th>Hard Drive</th>
                <th>Memory</th>
                <th>Operating System</th>
                <th>Work Group</th>
                <th>Ip Address</th>
                <th>Bios</th>
                <th>Firmware</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetList.map((asset, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${asset.id}`} color="link" size="sm">
                      {asset.id}
                    </Button>
                  </td>
                  <td>{asset.name}</td>
                  <td>{asset.tag}</td>
                  <td>{asset.customTag}</td>
                  <td>{asset.description}</td>
                  <td>{asset.statusId}</td>
                  <td>
                    {asset.purchaseDate ? <TextFormat type="date" value={asset.purchaseDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{asset.purchaseFrom}</td>
                  <td>{asset.purchaseCost}</td>
                  <td>{asset.brand}</td>
                  <td>{asset.model}</td>
                  <td>{asset.serialNo}</td>
                  <td>{asset.siteId}</td>
                  <td>{asset.locationId}</td>
                  <td>{asset.departmentId}</td>
                  <td>{asset.categoryId}</td>
                  <td>
                    {asset.photo ? (
                      <div>
                        {asset.photoContentType ? (
                          <a onClick={openFile(asset.photoContentType, asset.photo)}>
                            <img src={`data:${asset.photoContentType};base64,${asset.photo}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {asset.photoContentType}, {byteSize(asset.photo)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{asset.eolDate ? <TextFormat type="date" value={asset.eolDate} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td>{asset.assetTypeId}</td>
                  <td>{asset.systemName}</td>
                  <td>{asset.processor}</td>
                  <td>{asset.hardDrive}</td>
                  <td>{asset.memory}</td>
                  <td>{asset.operatingSystem}</td>
                  <td>{asset.workGroup}</td>
                  <td>{asset.ipAddress}</td>
                  <td>{asset.bios}</td>
                  <td>{asset.firmware}</td>
                  <td>{asset.comments}</td>
                  <td>{asset.businessId}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${asset.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${asset.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${asset.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Assets found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ asset }: IRootState) => ({
  assetList: asset.entities,
  loading: asset.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Asset);
