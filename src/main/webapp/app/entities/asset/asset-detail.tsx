import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './asset.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { Table } from 'reactstrap';

// import Tab from '@bit/react-bootstrap.react-bootstrap.tab'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
// import Tabs from '@bit/react-bootstrap.react-bootstrap.tabs'

export interface IAssetDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> { }

export const AssetDetail = (props: IAssetDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { assetEntity, match, loading } = props;
  return (
    <Row>
      <Col md="12">
        <h2>
          Asset [<b>{assetEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{assetEntity.name}</dd>
          <dt>
            <span id="tag">Tag</span>
          </dt>
          <dd>{assetEntity.tag}</dd>
          <dt>
            <span id="customTag">Custom Tag</span>
          </dt>
          <dd>{assetEntity.customTag}</dd>
          <dt>
            <span id="description">Description</span>
          </dt>
          <dd>{assetEntity.description}</dd>
          <dt>
            <span id="statusId">Status Id</span>
          </dt>
          <dd>{assetEntity.statusId}</dd>
          <dt>
            <span id="purchaseDate">Purchase Date</span>
          </dt>
          <dd>
            {assetEntity.purchaseDate ? <TextFormat value={assetEntity.purchaseDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="purchaseFrom">Purchase From</span>
          </dt>
          <dd>{assetEntity.purchaseFrom}</dd>
          <dt>
            <span id="purchaseCost">Purchase Cost</span>
          </dt>
          <dd>{assetEntity.purchaseCost}</dd>
          <dt>
            <span id="brand">Brand</span>
          </dt>
          <dd>{assetEntity.brand}</dd>
          <dt>
            <span id="model">Model</span>
          </dt>
          <dd>{assetEntity.model}</dd>
          <dt>
            <span id="serialNo">Serial No</span>
          </dt>
          <dd>{assetEntity.serialNo}</dd>
          <dt>
            <span id="siteId">Site Id</span>
          </dt>
          <dd>{assetEntity.siteId}</dd>
          <dt>
            <span id="locationId">Location Id</span>
          </dt>
          <dd>{assetEntity.locationId}</dd>
          <dt>
            <span id="departmentId">Department Id</span>
          </dt>
          <dd>{assetEntity.departmentId}</dd>
          <dt>
            <span id="categoryId">Category Id</span>
          </dt>
          <dd>{assetEntity.categoryId}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {assetEntity.photo ? (
              <div>
                {assetEntity.photoContentType ? (
                  <a onClick={openFile(assetEntity.photoContentType, assetEntity.photo)}>
                    <img src={`data:${assetEntity.photoContentType};base64,${assetEntity.photo}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {assetEntity.photoContentType}, {byteSize(assetEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="eolDate">Eol Date</span>
          </dt>
          <dd>{assetEntity.eolDate ? <TextFormat value={assetEntity.eolDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="assetTypeId">Asset Type Id</span>
          </dt>
          <dd>{assetEntity.assetTypeId}</dd>
          <dt>
            <span id="systemName">System Name</span>
          </dt>
          <dd>{assetEntity.systemName}</dd>
          <dt>
            <span id="processor">Processor</span>
          </dt>
          <dd>{assetEntity.processor}</dd>
          <dt>
            <span id="hardDrive">Hard Drive</span>
          </dt>
          <dd>{assetEntity.hardDrive}</dd>
          <dt>
            <span id="memory">Memory</span>
          </dt>
          <dd>{assetEntity.memory}</dd>
          <dt>
            <span id="operatingSystem">Operating System</span>
          </dt>
          <dd>{assetEntity.operatingSystem}</dd>
          <dt>
            <span id="workGroup">Work Group</span>
          </dt>
          <dd>{assetEntity.workGroup}</dd>
          <dt>
            <span id="ipAddress">Ip Address</span>
          </dt>
          <dd>{assetEntity.ipAddress}</dd>
          <dt>
            <span id="bios">Bios</span>
          </dt>
          <dd>{assetEntity.bios}</dd>
          <dt>
            <span id="firmware">Firmware</span>
          </dt>
          <dd>{assetEntity.firmware}</dd>
          <dt>
            <span id="comments">Comments</span>
          </dt>
          <dd>{assetEntity.comments}</dd>
          <dt>
            <span id="businessId">Business Id</span>
          </dt>
          <dd>{assetEntity.businessId}</dd>
        </dl>
        <Tabs>
          <TabList>
            <Tab>Check Out</Tab>
            <Tab>Check In</Tab>
          </TabList>
          <TabPanel>
          <div className="table-responsive">
        {assetEntity.checkOuts && assetEntity.checkOuts.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Status Id</th>
                <th>Check Out To</th>
                <th>Assigned To</th>
                <th>Check Out Date</th>
                <th>Check In From</th>
                <th>Returned From</th>
                <th>Check In Date</th>
                <th>Lease Start Date</th>
                <th>Lease From Customer Id</th>
                <th>Lease End Date</th>
                <th>Lease Return Date</th>
                <th>Lost Date</th>
                <th>Found Date</th>
                <th>Repair Schedule Date</th>
                <th>Repair Assigned To</th>
                <th>Repair Completed Date</th>
                <th>Repair Cost</th>
                <th>Broken Date</th>
                <th>Dispose Date</th>
                <th>Dispose To</th>
                <th>Donate Date</th>
                <th>Donate To</th>
                <th>Donate Value</th>
                <th>Deductible</th>
                <th>Sell Date</th>
                <th>Sell To</th>
                <th>Sell Value</th>
                <th>Is Due Date</th>
                <th>Due Date</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th>Asset</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetEntity.checkOuts.map((assetEvent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="link" size="sm">
                      {assetEvent.id}
                    </Button>
                  </td>
                  <td>{assetEvent.statusId}</td>
                  <td>{assetEvent.checkOutTo}</td>
                  <td>{assetEvent.assignedTo}</td>
                  <td>
                    {assetEvent.checkOutDate ? (
                      <TextFormat type="date" value={assetEvent.checkOutDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.checkInFrom}</td>
                  <td>{assetEvent.returnedFrom}</td>
                  <td>
                    {assetEvent.checkInDate ? (
                      <TextFormat type="date" value={assetEvent.checkInDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseStartDate ? (
                      <TextFormat type="date" value={assetEvent.leaseStartDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.leaseFromCustomerId}</td>
                  <td>
                    {assetEvent.leaseEndDate ? (
                      <TextFormat type="date" value={assetEvent.leaseEndDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseReturnDate ? (
                      <TextFormat type="date" value={assetEvent.leaseReturnDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.lostDate ? <TextFormat type="date" value={assetEvent.lostDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.foundDate ? <TextFormat type="date" value={assetEvent.foundDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.repairScheduleDate ? (
                      <TextFormat type="date" value={assetEvent.repairScheduleDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairAssignedTo}</td>
                  <td>
                    {assetEvent.repairCompletedDate ? (
                      <TextFormat type="date" value={assetEvent.repairCompletedDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairCost}</td>
                  <td>
                    {assetEvent.brokenDate ? <TextFormat type="date" value={assetEvent.brokenDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.disposeDate ? (
                      <TextFormat type="date" value={assetEvent.disposeDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.disposeTo}</td>
                  <td>
                    {assetEvent.donateDate ? <TextFormat type="date" value={assetEvent.donateDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.donateTo}</td>
                  <td>{assetEvent.donateValue}</td>
                  <td>{assetEvent.deductible ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.sellDate ? <TextFormat type="date" value={assetEvent.sellDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.sellTo}</td>
                  <td>{assetEvent.sellValue}</td>
                  <td>{assetEvent.isDueDate ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.dueDate ? <TextFormat type="date" value={assetEvent.dueDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.siteId}</td>
                  <td>{assetEvent.locationId}</td>
                  <td>{assetEvent.comments}</td>
                  <td>{assetEvent.businessId}</td>
                  <td>{assetEvent.assetId ? <Link to={`asset/${assetEvent.assetId}`}>{assetEvent.assetId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/${assetEvent.assetId}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Check Out Asset Events found</div>
        )}
      </div>
          </TabPanel>
          <TabPanel>
          <div className="table-responsive">
        {assetEntity.checkIns && assetEntity.checkIns.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Status Id</th>
                <th>Check Out To</th>
                <th>Assigned To</th>
                <th>Check Out Date</th>
                <th>Check In From</th>
                <th>Returned From</th>
                <th>Check In Date</th>
                <th>Lease Start Date</th>
                <th>Lease From Customer Id</th>
                <th>Lease End Date</th>
                <th>Lease Return Date</th>
                <th>Lost Date</th>
                <th>Found Date</th>
                <th>Repair Schedule Date</th>
                <th>Repair Assigned To</th>
                <th>Repair Completed Date</th>
                <th>Repair Cost</th>
                <th>Broken Date</th>
                <th>Dispose Date</th>
                <th>Dispose To</th>
                <th>Donate Date</th>
                <th>Donate To</th>
                <th>Donate Value</th>
                <th>Deductible</th>
                <th>Sell Date</th>
                <th>Sell To</th>
                <th>Sell Value</th>
                <th>Is Due Date</th>
                <th>Due Date</th>
                <th>Site Id</th>
                <th>Location Id</th>
                <th>Comments</th>
                <th>Business Id</th>
                <th>Asset</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {assetEntity.checkIns.map((assetEvent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                  <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="link" size="sm">
                      {assetEvent.id}
                    </Button>
                  </td>
                  <td>{assetEvent.statusId}</td>
                  <td>{assetEvent.checkOutTo}</td>
                  <td>{assetEvent.assignedTo}</td>
                  <td>
                    {assetEvent.checkOutDate ? (
                      <TextFormat type="date" value={assetEvent.checkOutDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.checkInFrom}</td>
                  <td>{assetEvent.returnedFrom}</td>
                  <td>
                    {assetEvent.checkInDate ? (
                      <TextFormat type="date" value={assetEvent.checkInDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseStartDate ? (
                      <TextFormat type="date" value={assetEvent.leaseStartDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.leaseFromCustomerId}</td>
                  <td>
                    {assetEvent.leaseEndDate ? (
                      <TextFormat type="date" value={assetEvent.leaseEndDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.leaseReturnDate ? (
                      <TextFormat type="date" value={assetEvent.leaseReturnDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {assetEvent.lostDate ? <TextFormat type="date" value={assetEvent.lostDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.foundDate ? <TextFormat type="date" value={assetEvent.foundDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.repairScheduleDate ? (
                      <TextFormat type="date" value={assetEvent.repairScheduleDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairAssignedTo}</td>
                  <td>
                    {assetEvent.repairCompletedDate ? (
                      <TextFormat type="date" value={assetEvent.repairCompletedDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.repairCost}</td>
                  <td>
                    {assetEvent.brokenDate ? <TextFormat type="date" value={assetEvent.brokenDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {assetEvent.disposeDate ? (
                      <TextFormat type="date" value={assetEvent.disposeDate} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{assetEvent.disposeTo}</td>
                  <td>
                    {assetEvent.donateDate ? <TextFormat type="date" value={assetEvent.donateDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.donateTo}</td>
                  <td>{assetEvent.donateValue}</td>
                  <td>{assetEvent.deductible ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.sellDate ? <TextFormat type="date" value={assetEvent.sellDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.sellTo}</td>
                  <td>{assetEvent.sellValue}</td>
                  <td>{assetEvent.isDueDate ? 'true' : 'false'}</td>
                  <td>
                    {assetEvent.dueDate ? <TextFormat type="date" value={assetEvent.dueDate} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{assetEvent.siteId}</td>
                  <td>{assetEvent.locationId}</td>
                  <td>{assetEvent.comments}</td>
                  <td>{assetEvent.businessId}</td>
                  <td>{assetEvent.assetId ? <Link to={`asset/${assetEvent.assetId}`}>{assetEvent.assetId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/${assetEvent.assetId}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/asset-event/${assetEvent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Check in Asset Events found</div>
        )}
      </div>
          
          </TabPanel>
        </Tabs>
        <Button tag={Link} to="/asset" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/asset/${assetEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
        <Link to={`/asset-event/${assetEntity.id}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Asset Event
        </Link>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ asset }: IRootState) => ({
  assetEntity: asset.entity,
  loading: asset.loading
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AssetDetail);
