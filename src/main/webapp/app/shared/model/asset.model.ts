import { Moment } from 'moment';

export interface IAsset {
  id?: number;
  name?: string;
  tag?: string;
  customTag?: string;
  description?: string;
  statusId?: number;
  purchaseDate?: string;
  purchaseFrom?: string;
  purchaseCost?: number;
  brand?: string;
  model?: string;
  serialNo?: string;
  siteId?: number;
  locationId?: number;
  departmentId?: number;
  categoryId?: number;
  photoContentType?: string;
  photo?: any;
  eolDate?: string;
  assetTypeId?: number;
  systemName?: string;
  processor?: string;
  hardDrive?: string;
  memory?: string;
  operatingSystem?: string;
  workGroup?: string;
  ipAddress?: string;
  bios?: string;
  firmware?: string;
  comments?: string;
  businessId?: number;
  checkOuts?: any;
  checkIns?: any;
}

export const defaultValue: Readonly<IAsset> = {};
