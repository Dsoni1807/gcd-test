import { Moment } from 'moment';

export interface IAssetEvent {
  id?: number;
  statusId?: number;
  checkOutTo?: number;
  assignedTo?: number;
  checkOutDate?: string;
  checkInFrom?: number;
  returnedFrom?: number;
  checkInDate?: string;
  leaseStartDate?: string;
  leaseFromCustomerId?: number;
  leaseEndDate?: string;
  leaseReturnDate?: string;
  lostDate?: string;
  foundDate?: string;
  repairScheduleDate?: string;
  repairAssignedTo?: string;
  repairCompletedDate?: string;
  repairCost?: number;
  brokenDate?: string;
  disposeDate?: string;
  disposeTo?: string;
  donateDate?: string;
  donateTo?: string;
  donateValue?: number;
  deductible?: boolean;
  sellDate?: string;
  sellTo?: string;
  sellValue?: number;
  isDueDate?: boolean;
  dueDate?: string;
  siteId?: number;
  locationId?: number;
  comments?: string;
  businessId?: number;
  assetId?: number;
}

export const defaultValue: Readonly<IAssetEvent> = {
  deductible: false,
  isDueDate: false,
};
