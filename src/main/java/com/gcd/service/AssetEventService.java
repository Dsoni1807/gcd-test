package com.gcd.service;

import com.gcd.service.dto.AssetEventDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.gcd.domain.AssetEvent}.
 */
public interface AssetEventService {

	/**
	 * Save a assetEvent.
	 *
	 * @param assetEventDTO the entity to save.
	 * @return the persisted entity.
	 */
	AssetEventDTO save(AssetEventDTO assetEventDTO);

	/**
	 * Get all the assetEvents.
	 *
	 * @return the list of entities.
	 */
	List<AssetEventDTO> findAll();

	/**
	 * Get the "id" assetEvent.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<AssetEventDTO> findOne(Long id);

	/**
	 * Delete the "id" assetEvent.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Get all the assetEvents check outs.
	 *
	 * @return the list of entities.
	 */
	List<AssetEventDTO> findAllCheckOuts(Long assetId);

	/**
	 * Get all the assetEvents check ins.
	 *
	 * @return the list of entities.
	 */
	List<AssetEventDTO> findAllCheckIns(Long assetId);

}
