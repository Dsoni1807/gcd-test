package com.gcd.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcd.domain.AssetEvent;
import com.gcd.repository.AssetEventRepository;
import com.gcd.service.AssetEventService;
import com.gcd.service.dto.AssetEventDTO;
import com.gcd.service.mapper.AssetEventMapper;

/**
 * Service Implementation for managing {@link AssetEvent}.
 */
@Service
@Transactional
public class AssetEventServiceImpl implements AssetEventService {

	private final Logger log = LoggerFactory.getLogger(AssetEventServiceImpl.class);

	private final AssetEventRepository assetEventRepository;

	private final AssetEventMapper assetEventMapper;

	public AssetEventServiceImpl(AssetEventRepository assetEventRepository, AssetEventMapper assetEventMapper) {
		this.assetEventRepository = assetEventRepository;
		this.assetEventMapper = assetEventMapper;
	}

	@Override
	public AssetEventDTO save(AssetEventDTO assetEventDTO) {
		log.debug("Request to save AssetEvent : {}", assetEventDTO);
		AssetEvent assetEvent = assetEventMapper.toEntity(assetEventDTO);
		assetEvent = assetEventRepository.save(assetEvent);
		return assetEventMapper.toDto(assetEvent);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AssetEventDTO> findAll() {
		log.debug("Request to get all AssetEvents");
		return assetEventRepository.findAll().stream().map(assetEventMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<AssetEventDTO> findOne(Long id) {
		log.debug("Request to get AssetEvent : {}", id);
		return assetEventRepository.findById(id).map(assetEventMapper::toDto);
	}

	@Override
	public void delete(Long id) {
		log.debug("Request to delete AssetEvent : {}", id);
		assetEventRepository.deleteById(id);
	}

	/**
	 * Get all the assetEvents check outs.
	 *
	 * @return the list of entities.
	 */

	@Override
	@Transactional(readOnly = true)
	public List<AssetEventDTO> findAllCheckOuts(Long assetId) {
		return assetEventRepository.findAllByAssetIdAndCheckOutToNotNull(assetId).stream().map(assetEventMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get all the assetEvents check ins.
	 *
	 * @return the list of entities.
	 */

	@Override
	@Transactional(readOnly = true)
	public List<AssetEventDTO> findAllCheckIns(Long assetId) {
		return assetEventRepository.findAllByAssetIdAndCheckInFromNotNull(assetId).stream().map(assetEventMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}
}
