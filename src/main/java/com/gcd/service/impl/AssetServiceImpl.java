package com.gcd.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcd.domain.Asset;
import com.gcd.repository.AssetRepository;
import com.gcd.service.AssetEventService;
import com.gcd.service.AssetService;
import com.gcd.service.dto.AssetDTO;
import com.gcd.service.mapper.AssetMapper;

/**
 * Service Implementation for managing {@link Asset}.
 */
@Service
@Transactional
public class AssetServiceImpl implements AssetService {

	private final Logger log = LoggerFactory.getLogger(AssetServiceImpl.class);

	private final AssetRepository assetRepository;

	private final AssetMapper assetMapper;

	@Autowired
	private AssetEventService assetEventService;

	public AssetServiceImpl(AssetRepository assetRepository, AssetMapper assetMapper) {
		this.assetRepository = assetRepository;
		this.assetMapper = assetMapper;
	}

	@Override
	public AssetDTO save(AssetDTO assetDTO) {
		log.debug("Request to save Asset : {}", assetDTO);
		Asset asset = assetMapper.toEntity(assetDTO);
		asset = assetRepository.save(asset);
		return assetMapper.toDto(asset);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AssetDTO> findAll() {
		log.debug("Request to get all Assets");
		return assetRepository.findAll().stream().map(assetMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<AssetDTO> findOne(Long id) {
		log.debug("Request to get Asset : {}", id);
		Optional<AssetDTO> assetDTO = assetRepository.findById(id).map(assetMapper::toDto);
		assetDTO.ifPresent(asset -> {
			Long assetId = asset.getId();
			asset.setCheckOuts(assetEventService.findAllCheckOuts(assetId));
			asset.setCheckIns(assetEventService.findAllCheckIns(assetId));
		});
		return assetDTO;
	}

	@Override
	public void delete(Long id) {
		log.debug("Request to delete Asset : {}", id);
		assetRepository.deleteById(id);
	}
}
