package com.gcd.service;

import com.gcd.service.dto.AssetDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.gcd.domain.Asset}.
 */
public interface AssetService {

    /**
     * Save a asset.
     *
     * @param assetDTO the entity to save.
     * @return the persisted entity.
     */
    AssetDTO save(AssetDTO assetDTO);

    /**
     * Get all the assets.
     *
     * @return the list of entities.
     */
    List<AssetDTO> findAll();


    /**
     * Get the "id" asset.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AssetDTO> findOne(Long id);

    /**
     * Delete the "id" asset.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
