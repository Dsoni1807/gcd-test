package com.gcd.service.mapper;


import com.gcd.domain.*;
import com.gcd.service.dto.AssetEventDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AssetEvent} and its DTO {@link AssetEventDTO}.
 */
@Mapper(componentModel = "spring", uses = {AssetMapper.class})
public interface AssetEventMapper extends EntityMapper<AssetEventDTO, AssetEvent> {

    @Mapping(source = "asset.id", target = "assetId")
    AssetEventDTO toDto(AssetEvent assetEvent);

    @Mapping(source = "assetId", target = "asset")
    AssetEvent toEntity(AssetEventDTO assetEventDTO);

    default AssetEvent fromId(Long id) {
        if (id == null) {
            return null;
        }
        AssetEvent assetEvent = new AssetEvent();
        assetEvent.setId(id);
        return assetEvent;
    }
}
