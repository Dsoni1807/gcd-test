package com.gcd.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the {@link com.gcd.domain.Asset} entity.
 */
public class AssetDTO implements Serializable {

	private Long id;

	@NotNull
	@Size(max = 250)
	private String name;

	@Size(max = 150)
	private String tag;

	@Size(max = 150)
	private String customTag;

	@Size(max = 500)
	private String description;

	@NotNull
	private Long statusId;

	@NotNull
	private LocalDate purchaseDate;

	@Size(max = 250)
	private String purchaseFrom;

	private BigDecimal purchaseCost;

	@Size(max = 250)
	private String brand;

	@Size(max = 250)
	private String model;

	@Size(max = 250)
	private String serialNo;

	private Long siteId;

	private Long locationId;

	private Long departmentId;

	private Long categoryId;

	@Lob
	private byte[] photo;

	private String photoContentType;
	private LocalDate eolDate;

	private Long assetTypeId;

	@Size(max = 250)
	private String systemName;

	@Size(max = 250)
	private String processor;

	@Size(max = 250)
	private String hardDrive;

	@Size(max = 250)
	private String memory;

	@Size(max = 250)
	private String operatingSystem;

	@Size(max = 250)
	private String workGroup;

	@Size(max = 250)
	private String ipAddress;

	@Size(max = 250)
	private String bios;

	@Size(max = 250)
	private String firmware;

	private String comments;

	private Long businessId;

	private List<AssetEventDTO> checkOuts;

	private List<AssetEventDTO> checkIns;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCustomTag() {
		return customTag;
	}

	public void setCustomTag(String customTag) {
		this.customTag = customTag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPurchaseFrom() {
		return purchaseFrom;
	}

	public void setPurchaseFrom(String purchaseFrom) {
		this.purchaseFrom = purchaseFrom;
	}

	public BigDecimal getPurchaseCost() {
		return purchaseCost;
	}

	public void setPurchaseCost(BigDecimal purchaseCost) {
		this.purchaseCost = purchaseCost;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoContentType() {
		return photoContentType;
	}

	public void setPhotoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
	}

	public LocalDate getEolDate() {
		return eolDate;
	}

	public void setEolDate(LocalDate eolDate) {
		this.eolDate = eolDate;
	}

	public Long getAssetTypeId() {
		return assetTypeId;
	}

	public void setAssetTypeId(Long assetTypeId) {
		this.assetTypeId = assetTypeId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getHardDrive() {
		return hardDrive;
	}

	public void setHardDrive(String hardDrive) {
		this.hardDrive = hardDrive;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getWorkGroup() {
		return workGroup;
	}

	public void setWorkGroup(String workGroup) {
		this.workGroup = workGroup;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getBios() {
		return bios;
	}

	public void setBios(String bios) {
		this.bios = bios;
	}

	public String getFirmware() {
		return firmware;
	}

	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public List<AssetEventDTO> getCheckOuts() {
		return checkOuts;
	}

	public void setCheckOuts(List<AssetEventDTO> checkOuts) {
		this.checkOuts = checkOuts;
	}

	public List<AssetEventDTO> getCheckIns() {
		return checkIns;
	}

	public void setCheckIns(List<AssetEventDTO> checkIns) {
		this.checkIns = checkIns;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AssetDTO)) {
			return false;
		}

		return id != null && id.equals(((AssetDTO) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	// prettier-ignore
	@Override
	public String toString() {
		return "AssetDTO{" + "id=" + getId() + ", name='" + getName() + "'" + ", tag='" + getTag() + "'"
				+ ", customTag='" + getCustomTag() + "'" + ", description='" + getDescription() + "'" + ", statusId="
				+ getStatusId() + ", purchaseDate='" + getPurchaseDate() + "'" + ", purchaseFrom='" + getPurchaseFrom()
				+ "'" + ", purchaseCost=" + getPurchaseCost() + ", brand='" + getBrand() + "'" + ", model='"
				+ getModel() + "'" + ", serialNo='" + getSerialNo() + "'" + ", siteId=" + getSiteId() + ", locationId="
				+ getLocationId() + ", departmentId=" + getDepartmentId() + ", categoryId=" + getCategoryId()
				+ ", photo='" + getPhoto() + "'" + ", eolDate='" + getEolDate() + "'" + ", assetTypeId="
				+ getAssetTypeId() + ", systemName='" + getSystemName() + "'" + ", processor='" + getProcessor() + "'"
				+ ", hardDrive='" + getHardDrive() + "'" + ", memory='" + getMemory() + "'" + ", operatingSystem='"
				+ getOperatingSystem() + "'" + ", workGroup='" + getWorkGroup() + "'" + ", ipAddress='" + getIpAddress()
				+ "'" + ", bios='" + getBios() + "'" + ", firmware='" + getFirmware() + "'" + ", comments='"
				+ getComments() + "'" + ", businessId=" + getBusinessId() + "}";
	}
}
