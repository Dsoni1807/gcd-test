package com.gcd.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link com.gcd.domain.AssetEvent} entity.
 */
public class AssetEventDTO implements Serializable {
    
    private Long id;

    private Long statusId;

    private Integer checkOutTo;

    private Long assignedTo;

    private LocalDate checkOutDate;

    private Integer checkInFrom;

    private Long returnedFrom;

    private LocalDate checkInDate;

    private LocalDate leaseStartDate;

    private Long leaseFromCustomerId;

    private LocalDate leaseEndDate;

    private LocalDate leaseReturnDate;

    private LocalDate lostDate;

    private LocalDate foundDate;

    private LocalDate repairScheduleDate;

    @Size(max = 250)
    private String repairAssignedTo;

    private LocalDate repairCompletedDate;

    private BigDecimal repairCost;

    private LocalDate brokenDate;

    private LocalDate disposeDate;

    private String disposeTo;

    private LocalDate donateDate;

    @Size(max = 250)
    private String donateTo;

    private BigDecimal donateValue;

    private Boolean deductible;

    private LocalDate sellDate;

    @Size(max = 250)
    private String sellTo;

    private BigDecimal sellValue;

    private Boolean isDueDate;

    private LocalDate dueDate;

    private Long siteId;

    private Long locationId;

    private String comments;

    private Long businessId;


    private Long assetId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Integer getCheckOutTo() {
        return checkOutTo;
    }

    public void setCheckOutTo(Integer checkOutTo) {
        this.checkOutTo = checkOutTo;
    }

    public Long getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Long assignedTo) {
        this.assignedTo = assignedTo;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public Integer getCheckInFrom() {
        return checkInFrom;
    }

    public void setCheckInFrom(Integer checkInFrom) {
        this.checkInFrom = checkInFrom;
    }

    public Long getReturnedFrom() {
        return returnedFrom;
    }

    public void setReturnedFrom(Long returnedFrom) {
        this.returnedFrom = returnedFrom;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public LocalDate getLeaseStartDate() {
        return leaseStartDate;
    }

    public void setLeaseStartDate(LocalDate leaseStartDate) {
        this.leaseStartDate = leaseStartDate;
    }

    public Long getLeaseFromCustomerId() {
        return leaseFromCustomerId;
    }

    public void setLeaseFromCustomerId(Long leaseFromCustomerId) {
        this.leaseFromCustomerId = leaseFromCustomerId;
    }

    public LocalDate getLeaseEndDate() {
        return leaseEndDate;
    }

    public void setLeaseEndDate(LocalDate leaseEndDate) {
        this.leaseEndDate = leaseEndDate;
    }

    public LocalDate getLeaseReturnDate() {
        return leaseReturnDate;
    }

    public void setLeaseReturnDate(LocalDate leaseReturnDate) {
        this.leaseReturnDate = leaseReturnDate;
    }

    public LocalDate getLostDate() {
        return lostDate;
    }

    public void setLostDate(LocalDate lostDate) {
        this.lostDate = lostDate;
    }

    public LocalDate getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(LocalDate foundDate) {
        this.foundDate = foundDate;
    }

    public LocalDate getRepairScheduleDate() {
        return repairScheduleDate;
    }

    public void setRepairScheduleDate(LocalDate repairScheduleDate) {
        this.repairScheduleDate = repairScheduleDate;
    }

    public String getRepairAssignedTo() {
        return repairAssignedTo;
    }

    public void setRepairAssignedTo(String repairAssignedTo) {
        this.repairAssignedTo = repairAssignedTo;
    }

    public LocalDate getRepairCompletedDate() {
        return repairCompletedDate;
    }

    public void setRepairCompletedDate(LocalDate repairCompletedDate) {
        this.repairCompletedDate = repairCompletedDate;
    }

    public BigDecimal getRepairCost() {
        return repairCost;
    }

    public void setRepairCost(BigDecimal repairCost) {
        this.repairCost = repairCost;
    }

    public LocalDate getBrokenDate() {
        return brokenDate;
    }

    public void setBrokenDate(LocalDate brokenDate) {
        this.brokenDate = brokenDate;
    }

    public LocalDate getDisposeDate() {
        return disposeDate;
    }

    public void setDisposeDate(LocalDate disposeDate) {
        this.disposeDate = disposeDate;
    }

    public String getDisposeTo() {
        return disposeTo;
    }

    public void setDisposeTo(String disposeTo) {
        this.disposeTo = disposeTo;
    }

    public LocalDate getDonateDate() {
        return donateDate;
    }

    public void setDonateDate(LocalDate donateDate) {
        this.donateDate = donateDate;
    }

    public String getDonateTo() {
        return donateTo;
    }

    public void setDonateTo(String donateTo) {
        this.donateTo = donateTo;
    }

    public BigDecimal getDonateValue() {
        return donateValue;
    }

    public void setDonateValue(BigDecimal donateValue) {
        this.donateValue = donateValue;
    }

    public Boolean isDeductible() {
        return deductible;
    }

    public void setDeductible(Boolean deductible) {
        this.deductible = deductible;
    }

    public LocalDate getSellDate() {
        return sellDate;
    }

    public void setSellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
    }

    public String getSellTo() {
        return sellTo;
    }

    public void setSellTo(String sellTo) {
        this.sellTo = sellTo;
    }

    public BigDecimal getSellValue() {
        return sellValue;
    }

    public void setSellValue(BigDecimal sellValue) {
        this.sellValue = sellValue;
    }

    public Boolean isIsDueDate() {
        return isDueDate;
    }

    public void setIsDueDate(Boolean isDueDate) {
        this.isDueDate = isDueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Long getAssetId() {
        return assetId;
    }

    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetEventDTO)) {
            return false;
        }

        return id != null && id.equals(((AssetEventDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssetEventDTO{" +
            "id=" + getId() +
            ", statusId=" + getStatusId() +
            ", checkOutTo=" + getCheckOutTo() +
            ", assignedTo=" + getAssignedTo() +
            ", checkOutDate='" + getCheckOutDate() + "'" +
            ", checkInFrom=" + getCheckInFrom() +
            ", returnedFrom=" + getReturnedFrom() +
            ", checkInDate='" + getCheckInDate() + "'" +
            ", leaseStartDate='" + getLeaseStartDate() + "'" +
            ", leaseFromCustomerId=" + getLeaseFromCustomerId() +
            ", leaseEndDate='" + getLeaseEndDate() + "'" +
            ", leaseReturnDate='" + getLeaseReturnDate() + "'" +
            ", lostDate='" + getLostDate() + "'" +
            ", foundDate='" + getFoundDate() + "'" +
            ", repairScheduleDate='" + getRepairScheduleDate() + "'" +
            ", repairAssignedTo='" + getRepairAssignedTo() + "'" +
            ", repairCompletedDate='" + getRepairCompletedDate() + "'" +
            ", repairCost=" + getRepairCost() +
            ", brokenDate='" + getBrokenDate() + "'" +
            ", disposeDate='" + getDisposeDate() + "'" +
            ", disposeTo='" + getDisposeTo() + "'" +
            ", donateDate='" + getDonateDate() + "'" +
            ", donateTo='" + getDonateTo() + "'" +
            ", donateValue=" + getDonateValue() +
            ", deductible='" + isDeductible() + "'" +
            ", sellDate='" + getSellDate() + "'" +
            ", sellTo='" + getSellTo() + "'" +
            ", sellValue=" + getSellValue() +
            ", isDueDate='" + isIsDueDate() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", siteId=" + getSiteId() +
            ", locationId=" + getLocationId() +
            ", comments='" + getComments() + "'" +
            ", businessId=" + getBusinessId() +
            ", assetId=" + getAssetId() +
            "}";
    }
}
