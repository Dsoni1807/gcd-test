package com.gcd.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A Asset.
 */
@Entity
@Table(name = "asset")
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 250)
    @Column(name = "name", length = 250, nullable = false)
    private String name;

    @Size(max = 150)
    @Column(name = "tag", length = 150)
    private String tag;

    @Size(max = 150)
    @Column(name = "custom_tag", length = 150)
    private String customTag;

    @Size(max = 500)
    @Column(name = "description", length = 500)
    private String description;

    @NotNull
    @Column(name = "status_id", nullable = false)
    private Long statusId;

    @NotNull
    @Column(name = "purchase_date", nullable = false)
    private LocalDate purchaseDate;

    @Size(max = 250)
    @Column(name = "purchase_from", length = 250)
    private String purchaseFrom;

    @Column(name = "purchase_cost", precision = 21, scale = 2)
    private BigDecimal purchaseCost;

    @Size(max = 250)
    @Column(name = "brand", length = 250)
    private String brand;

    @Size(max = 250)
    @Column(name = "model", length = 250)
    private String model;

    @Size(max = 250)
    @Column(name = "serial_no", length = 250)
    private String serialNo;

    @Column(name = "site_id")
    private Long siteId;

    @Column(name = "location_id")
    private Long locationId;

    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "category_id")
    private Long categoryId;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @Column(name = "eol_date")
    private LocalDate eolDate;

    @Column(name = "asset_type_id")
    private Long assetTypeId;

    @Size(max = 250)
    @Column(name = "system_name", length = 250)
    private String systemName;

    @Size(max = 250)
    @Column(name = "processor", length = 250)
    private String processor;

    @Size(max = 250)
    @Column(name = "hard_drive", length = 250)
    private String hardDrive;

    @Size(max = 250)
    @Column(name = "memory", length = 250)
    private String memory;

    @Size(max = 250)
    @Column(name = "operating_system", length = 250)
    private String operatingSystem;

    @Size(max = 250)
    @Column(name = "work_group", length = 250)
    private String workGroup;

    @Size(max = 250)
    @Column(name = "ip_address", length = 250)
    private String ipAddress;

    @Size(max = 250)
    @Column(name = "bios", length = 250)
    private String bios;

    @Size(max = 250)
    @Column(name = "firmware", length = 250)
    private String firmware;

    @Column(name = "comments")
    private String comments;

    @Column(name = "business_id")
    private Long businessId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Asset name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public Asset tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCustomTag() {
        return customTag;
    }

    public Asset customTag(String customTag) {
        this.customTag = customTag;
        return this;
    }

    public void setCustomTag(String customTag) {
        this.customTag = customTag;
    }

    public String getDescription() {
        return description;
    }

    public Asset description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatusId() {
        return statusId;
    }

    public Asset statusId(Long statusId) {
        this.statusId = statusId;
        return this;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public Asset purchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
        return this;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseFrom() {
        return purchaseFrom;
    }

    public Asset purchaseFrom(String purchaseFrom) {
        this.purchaseFrom = purchaseFrom;
        return this;
    }

    public void setPurchaseFrom(String purchaseFrom) {
        this.purchaseFrom = purchaseFrom;
    }

    public BigDecimal getPurchaseCost() {
        return purchaseCost;
    }

    public Asset purchaseCost(BigDecimal purchaseCost) {
        this.purchaseCost = purchaseCost;
        return this;
    }

    public void setPurchaseCost(BigDecimal purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public String getBrand() {
        return brand;
    }

    public Asset brand(String brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public Asset model(String model) {
        this.model = model;
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public Asset serialNo(String serialNo) {
        this.serialNo = serialNo;
        return this;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Long getSiteId() {
        return siteId;
    }

    public Asset siteId(Long siteId) {
        this.siteId = siteId;
        return this;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public Asset locationId(Long locationId) {
        this.locationId = locationId;
        return this;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public Asset departmentId(Long departmentId) {
        this.departmentId = departmentId;
        return this;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Asset categoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Asset photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Asset photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public LocalDate getEolDate() {
        return eolDate;
    }

    public Asset eolDate(LocalDate eolDate) {
        this.eolDate = eolDate;
        return this;
    }

    public void setEolDate(LocalDate eolDate) {
        this.eolDate = eolDate;
    }

    public Long getAssetTypeId() {
        return assetTypeId;
    }

    public Asset assetTypeId(Long assetTypeId) {
        this.assetTypeId = assetTypeId;
        return this;
    }

    public void setAssetTypeId(Long assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public String getSystemName() {
        return systemName;
    }

    public Asset systemName(String systemName) {
        this.systemName = systemName;
        return this;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getProcessor() {
        return processor;
    }

    public Asset processor(String processor) {
        this.processor = processor;
        return this;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getHardDrive() {
        return hardDrive;
    }

    public Asset hardDrive(String hardDrive) {
        this.hardDrive = hardDrive;
        return this;
    }

    public void setHardDrive(String hardDrive) {
        this.hardDrive = hardDrive;
    }

    public String getMemory() {
        return memory;
    }

    public Asset memory(String memory) {
        this.memory = memory;
        return this;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public Asset operatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
        return this;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getWorkGroup() {
        return workGroup;
    }

    public Asset workGroup(String workGroup) {
        this.workGroup = workGroup;
        return this;
    }

    public void setWorkGroup(String workGroup) {
        this.workGroup = workGroup;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Asset ipAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getBios() {
        return bios;
    }

    public Asset bios(String bios) {
        this.bios = bios;
        return this;
    }

    public void setBios(String bios) {
        this.bios = bios;
    }

    public String getFirmware() {
        return firmware;
    }

    public Asset firmware(String firmware) {
        this.firmware = firmware;
        return this;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getComments() {
        return comments;
    }

    public Asset comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public Asset businessId(Long businessId) {
        this.businessId = businessId;
        return this;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asset)) {
            return false;
        }
        return id != null && id.equals(((Asset) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asset{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", tag='" + getTag() + "'" +
            ", customTag='" + getCustomTag() + "'" +
            ", description='" + getDescription() + "'" +
            ", statusId=" + getStatusId() +
            ", purchaseDate='" + getPurchaseDate() + "'" +
            ", purchaseFrom='" + getPurchaseFrom() + "'" +
            ", purchaseCost=" + getPurchaseCost() +
            ", brand='" + getBrand() + "'" +
            ", model='" + getModel() + "'" +
            ", serialNo='" + getSerialNo() + "'" +
            ", siteId=" + getSiteId() +
            ", locationId=" + getLocationId() +
            ", departmentId=" + getDepartmentId() +
            ", categoryId=" + getCategoryId() +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", eolDate='" + getEolDate() + "'" +
            ", assetTypeId=" + getAssetTypeId() +
            ", systemName='" + getSystemName() + "'" +
            ", processor='" + getProcessor() + "'" +
            ", hardDrive='" + getHardDrive() + "'" +
            ", memory='" + getMemory() + "'" +
            ", operatingSystem='" + getOperatingSystem() + "'" +
            ", workGroup='" + getWorkGroup() + "'" +
            ", ipAddress='" + getIpAddress() + "'" +
            ", bios='" + getBios() + "'" +
            ", firmware='" + getFirmware() + "'" +
            ", comments='" + getComments() + "'" +
            ", businessId=" + getBusinessId() +
            "}";
    }
}
