package com.gcd.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A AssetEvent.
 */
@Entity
@Table(name = "asset_event")
public class AssetEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "check_out_to")
    private Integer checkOutTo;

    @Column(name = "assigned_to")
    private Long assignedTo;

    @Column(name = "check_out_date")
    private LocalDate checkOutDate;

    @Column(name = "check_in_from")
    private Integer checkInFrom;

    @Column(name = "returned_from")
    private Long returnedFrom;

    @Column(name = "check_in_date")
    private LocalDate checkInDate;

    @Column(name = "lease_start_date")
    private LocalDate leaseStartDate;

    @Column(name = "lease_from_customer_id")
    private Long leaseFromCustomerId;

    @Column(name = "lease_end_date")
    private LocalDate leaseEndDate;

    @Column(name = "lease_return_date")
    private LocalDate leaseReturnDate;

    @Column(name = "lost_date")
    private LocalDate lostDate;

    @Column(name = "found_date")
    private LocalDate foundDate;

    @Column(name = "repair_schedule_date")
    private LocalDate repairScheduleDate;

    @Size(max = 250)
    @Column(name = "repair_assigned_to", length = 250)
    private String repairAssignedTo;

    @Column(name = "repair_completed_date")
    private LocalDate repairCompletedDate;

    @Column(name = "repair_cost", precision = 21, scale = 2)
    private BigDecimal repairCost;

    @Column(name = "broken_date")
    private LocalDate brokenDate;

    @Column(name = "dispose_date")
    private LocalDate disposeDate;

    @Column(name = "dispose_to")
    private String disposeTo;

    @Column(name = "donate_date")
    private LocalDate donateDate;

    @Size(max = 250)
    @Column(name = "donate_to", length = 250)
    private String donateTo;

    @Column(name = "donate_value", precision = 21, scale = 2)
    private BigDecimal donateValue;

    @Column(name = "deductible")
    private Boolean deductible;

    @Column(name = "sell_date")
    private LocalDate sellDate;

    @Size(max = 250)
    @Column(name = "sell_to", length = 250)
    private String sellTo;

    @Column(name = "sell_value", precision = 21, scale = 2)
    private BigDecimal sellValue;

    @Column(name = "is_due_date")
    private Boolean isDueDate;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "site_id")
    private Long siteId;

    @Column(name = "location_id")
    private Long locationId;

    @Column(name = "comments")
    private String comments;

    @Column(name = "business_id")
    private Long businessId;

    @ManyToOne
    @JsonIgnoreProperties(value = "assetEvents", allowSetters = true)
    private Asset asset;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatusId() {
        return statusId;
    }

    public AssetEvent statusId(Long statusId) {
        this.statusId = statusId;
        return this;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Integer getCheckOutTo() {
        return checkOutTo;
    }

    public AssetEvent checkOutTo(Integer checkOutTo) {
        this.checkOutTo = checkOutTo;
        return this;
    }

    public void setCheckOutTo(Integer checkOutTo) {
        this.checkOutTo = checkOutTo;
    }

    public Long getAssignedTo() {
        return assignedTo;
    }

    public AssetEvent assignedTo(Long assignedTo) {
        this.assignedTo = assignedTo;
        return this;
    }

    public void setAssignedTo(Long assignedTo) {
        this.assignedTo = assignedTo;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public AssetEvent checkOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public Integer getCheckInFrom() {
        return checkInFrom;
    }

    public AssetEvent checkInFrom(Integer checkInFrom) {
        this.checkInFrom = checkInFrom;
        return this;
    }

    public void setCheckInFrom(Integer checkInFrom) {
        this.checkInFrom = checkInFrom;
    }

    public Long getReturnedFrom() {
        return returnedFrom;
    }

    public AssetEvent returnedFrom(Long returnedFrom) {
        this.returnedFrom = returnedFrom;
        return this;
    }

    public void setReturnedFrom(Long returnedFrom) {
        this.returnedFrom = returnedFrom;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public AssetEvent checkInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public LocalDate getLeaseStartDate() {
        return leaseStartDate;
    }

    public AssetEvent leaseStartDate(LocalDate leaseStartDate) {
        this.leaseStartDate = leaseStartDate;
        return this;
    }

    public void setLeaseStartDate(LocalDate leaseStartDate) {
        this.leaseStartDate = leaseStartDate;
    }

    public Long getLeaseFromCustomerId() {
        return leaseFromCustomerId;
    }

    public AssetEvent leaseFromCustomerId(Long leaseFromCustomerId) {
        this.leaseFromCustomerId = leaseFromCustomerId;
        return this;
    }

    public void setLeaseFromCustomerId(Long leaseFromCustomerId) {
        this.leaseFromCustomerId = leaseFromCustomerId;
    }

    public LocalDate getLeaseEndDate() {
        return leaseEndDate;
    }

    public AssetEvent leaseEndDate(LocalDate leaseEndDate) {
        this.leaseEndDate = leaseEndDate;
        return this;
    }

    public void setLeaseEndDate(LocalDate leaseEndDate) {
        this.leaseEndDate = leaseEndDate;
    }

    public LocalDate getLeaseReturnDate() {
        return leaseReturnDate;
    }

    public AssetEvent leaseReturnDate(LocalDate leaseReturnDate) {
        this.leaseReturnDate = leaseReturnDate;
        return this;
    }

    public void setLeaseReturnDate(LocalDate leaseReturnDate) {
        this.leaseReturnDate = leaseReturnDate;
    }

    public LocalDate getLostDate() {
        return lostDate;
    }

    public AssetEvent lostDate(LocalDate lostDate) {
        this.lostDate = lostDate;
        return this;
    }

    public void setLostDate(LocalDate lostDate) {
        this.lostDate = lostDate;
    }

    public LocalDate getFoundDate() {
        return foundDate;
    }

    public AssetEvent foundDate(LocalDate foundDate) {
        this.foundDate = foundDate;
        return this;
    }

    public void setFoundDate(LocalDate foundDate) {
        this.foundDate = foundDate;
    }

    public LocalDate getRepairScheduleDate() {
        return repairScheduleDate;
    }

    public AssetEvent repairScheduleDate(LocalDate repairScheduleDate) {
        this.repairScheduleDate = repairScheduleDate;
        return this;
    }

    public void setRepairScheduleDate(LocalDate repairScheduleDate) {
        this.repairScheduleDate = repairScheduleDate;
    }

    public String getRepairAssignedTo() {
        return repairAssignedTo;
    }

    public AssetEvent repairAssignedTo(String repairAssignedTo) {
        this.repairAssignedTo = repairAssignedTo;
        return this;
    }

    public void setRepairAssignedTo(String repairAssignedTo) {
        this.repairAssignedTo = repairAssignedTo;
    }

    public LocalDate getRepairCompletedDate() {
        return repairCompletedDate;
    }

    public AssetEvent repairCompletedDate(LocalDate repairCompletedDate) {
        this.repairCompletedDate = repairCompletedDate;
        return this;
    }

    public void setRepairCompletedDate(LocalDate repairCompletedDate) {
        this.repairCompletedDate = repairCompletedDate;
    }

    public BigDecimal getRepairCost() {
        return repairCost;
    }

    public AssetEvent repairCost(BigDecimal repairCost) {
        this.repairCost = repairCost;
        return this;
    }

    public void setRepairCost(BigDecimal repairCost) {
        this.repairCost = repairCost;
    }

    public LocalDate getBrokenDate() {
        return brokenDate;
    }

    public AssetEvent brokenDate(LocalDate brokenDate) {
        this.brokenDate = brokenDate;
        return this;
    }

    public void setBrokenDate(LocalDate brokenDate) {
        this.brokenDate = brokenDate;
    }

    public LocalDate getDisposeDate() {
        return disposeDate;
    }

    public AssetEvent disposeDate(LocalDate disposeDate) {
        this.disposeDate = disposeDate;
        return this;
    }

    public void setDisposeDate(LocalDate disposeDate) {
        this.disposeDate = disposeDate;
    }

    public String getDisposeTo() {
        return disposeTo;
    }

    public AssetEvent disposeTo(String disposeTo) {
        this.disposeTo = disposeTo;
        return this;
    }

    public void setDisposeTo(String disposeTo) {
        this.disposeTo = disposeTo;
    }

    public LocalDate getDonateDate() {
        return donateDate;
    }

    public AssetEvent donateDate(LocalDate donateDate) {
        this.donateDate = donateDate;
        return this;
    }

    public void setDonateDate(LocalDate donateDate) {
        this.donateDate = donateDate;
    }

    public String getDonateTo() {
        return donateTo;
    }

    public AssetEvent donateTo(String donateTo) {
        this.donateTo = donateTo;
        return this;
    }

    public void setDonateTo(String donateTo) {
        this.donateTo = donateTo;
    }

    public BigDecimal getDonateValue() {
        return donateValue;
    }

    public AssetEvent donateValue(BigDecimal donateValue) {
        this.donateValue = donateValue;
        return this;
    }

    public void setDonateValue(BigDecimal donateValue) {
        this.donateValue = donateValue;
    }

    public Boolean isDeductible() {
        return deductible;
    }

    public AssetEvent deductible(Boolean deductible) {
        this.deductible = deductible;
        return this;
    }

    public void setDeductible(Boolean deductible) {
        this.deductible = deductible;
    }

    public LocalDate getSellDate() {
        return sellDate;
    }

    public AssetEvent sellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
        return this;
    }

    public void setSellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
    }

    public String getSellTo() {
        return sellTo;
    }

    public AssetEvent sellTo(String sellTo) {
        this.sellTo = sellTo;
        return this;
    }

    public void setSellTo(String sellTo) {
        this.sellTo = sellTo;
    }

    public BigDecimal getSellValue() {
        return sellValue;
    }

    public AssetEvent sellValue(BigDecimal sellValue) {
        this.sellValue = sellValue;
        return this;
    }

    public void setSellValue(BigDecimal sellValue) {
        this.sellValue = sellValue;
    }

    public Boolean isIsDueDate() {
        return isDueDate;
    }

    public AssetEvent isDueDate(Boolean isDueDate) {
        this.isDueDate = isDueDate;
        return this;
    }

    public void setIsDueDate(Boolean isDueDate) {
        this.isDueDate = isDueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public AssetEvent dueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getSiteId() {
        return siteId;
    }

    public AssetEvent siteId(Long siteId) {
        this.siteId = siteId;
        return this;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public AssetEvent locationId(Long locationId) {
        this.locationId = locationId;
        return this;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getComments() {
        return comments;
    }

    public AssetEvent comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public AssetEvent businessId(Long businessId) {
        this.businessId = businessId;
        return this;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Asset getAsset() {
        return asset;
    }

    public AssetEvent asset(Asset asset) {
        this.asset = asset;
        return this;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetEvent)) {
            return false;
        }
        return id != null && id.equals(((AssetEvent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssetEvent{" +
            "id=" + getId() +
            ", statusId=" + getStatusId() +
            ", checkOutTo=" + getCheckOutTo() +
            ", assignedTo=" + getAssignedTo() +
            ", checkOutDate='" + getCheckOutDate() + "'" +
            ", checkInFrom=" + getCheckInFrom() +
            ", returnedFrom=" + getReturnedFrom() +
            ", checkInDate='" + getCheckInDate() + "'" +
            ", leaseStartDate='" + getLeaseStartDate() + "'" +
            ", leaseFromCustomerId=" + getLeaseFromCustomerId() +
            ", leaseEndDate='" + getLeaseEndDate() + "'" +
            ", leaseReturnDate='" + getLeaseReturnDate() + "'" +
            ", lostDate='" + getLostDate() + "'" +
            ", foundDate='" + getFoundDate() + "'" +
            ", repairScheduleDate='" + getRepairScheduleDate() + "'" +
            ", repairAssignedTo='" + getRepairAssignedTo() + "'" +
            ", repairCompletedDate='" + getRepairCompletedDate() + "'" +
            ", repairCost=" + getRepairCost() +
            ", brokenDate='" + getBrokenDate() + "'" +
            ", disposeDate='" + getDisposeDate() + "'" +
            ", disposeTo='" + getDisposeTo() + "'" +
            ", donateDate='" + getDonateDate() + "'" +
            ", donateTo='" + getDonateTo() + "'" +
            ", donateValue=" + getDonateValue() +
            ", deductible='" + isDeductible() + "'" +
            ", sellDate='" + getSellDate() + "'" +
            ", sellTo='" + getSellTo() + "'" +
            ", sellValue=" + getSellValue() +
            ", isDueDate='" + isIsDueDate() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", siteId=" + getSiteId() +
            ", locationId=" + getLocationId() +
            ", comments='" + getComments() + "'" +
            ", businessId=" + getBusinessId() +
            "}";
    }
}
