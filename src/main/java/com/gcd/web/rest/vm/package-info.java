/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gcd.web.rest.vm;
