package com.gcd.web.rest;

import com.gcd.service.AssetEventService;
import com.gcd.web.rest.errors.BadRequestAlertException;
import com.gcd.service.dto.AssetEventDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.gcd.domain.AssetEvent}.
 */
@RestController
@RequestMapping("/api")
public class AssetEventResource {

    private final Logger log = LoggerFactory.getLogger(AssetEventResource.class);

    private static final String ENTITY_NAME = "assetEvent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssetEventService assetEventService;

    public AssetEventResource(AssetEventService assetEventService) {
        this.assetEventService = assetEventService;
    }

    /**
     * {@code POST  /asset-events} : Create a new assetEvent.
     *
     * @param assetEventDTO the assetEventDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assetEventDTO, or with status {@code 400 (Bad Request)} if the assetEvent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/asset-events")
    public ResponseEntity<AssetEventDTO> createAssetEvent(@Valid @RequestBody AssetEventDTO assetEventDTO) throws URISyntaxException {
        log.debug("REST request to save AssetEvent : {}", assetEventDTO);
        if (assetEventDTO.getId() != null) {
            throw new BadRequestAlertException("A new assetEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AssetEventDTO result = assetEventService.save(assetEventDTO);
        return ResponseEntity.created(new URI("/api/asset-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /asset-events} : Updates an existing assetEvent.
     *
     * @param assetEventDTO the assetEventDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetEventDTO,
     * or with status {@code 400 (Bad Request)} if the assetEventDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assetEventDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/asset-events")
    public ResponseEntity<AssetEventDTO> updateAssetEvent(@Valid @RequestBody AssetEventDTO assetEventDTO) throws URISyntaxException {
        log.debug("REST request to update AssetEvent : {}", assetEventDTO);
        if (assetEventDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AssetEventDTO result = assetEventService.save(assetEventDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetEventDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /asset-events} : get all the assetEvents.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assetEvents in body.
     */
    @GetMapping("/asset-events")
    public List<AssetEventDTO> getAllAssetEvents() {
        log.debug("REST request to get all AssetEvents");
        return assetEventService.findAll();
    }

    /**
     * {@code GET  /asset-events/:id} : get the "id" assetEvent.
     *
     * @param id the id of the assetEventDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assetEventDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/asset-events/{id}")
    public ResponseEntity<AssetEventDTO> getAssetEvent(@PathVariable Long id) {
        log.debug("REST request to get AssetEvent : {}", id);
        Optional<AssetEventDTO> assetEventDTO = assetEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(assetEventDTO);
    }

    /**
     * {@code DELETE  /asset-events/:id} : delete the "id" assetEvent.
     *
     * @param id the id of the assetEventDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/asset-events/{id}")
    public ResponseEntity<Void> deleteAssetEvent(@PathVariable Long id) {
        log.debug("REST request to delete AssetEvent : {}", id);
        assetEventService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
