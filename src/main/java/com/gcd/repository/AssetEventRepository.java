package com.gcd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gcd.domain.AssetEvent;

/**
 * Spring Data  repository for the AssetEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetEventRepository extends JpaRepository<AssetEvent, Long> {
	List<AssetEvent> findAllByAssetIdAndCheckOutToNotNull(Long assetId);

	List<AssetEvent> findAllByAssetIdAndCheckInFromNotNull(Long assetId);
}
