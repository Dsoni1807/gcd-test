package com.gcd.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.gcd.web.rest.TestUtil;

public class AssetEventTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssetEvent.class);
        AssetEvent assetEvent1 = new AssetEvent();
        assetEvent1.setId(1L);
        AssetEvent assetEvent2 = new AssetEvent();
        assetEvent2.setId(assetEvent1.getId());
        assertThat(assetEvent1).isEqualTo(assetEvent2);
        assetEvent2.setId(2L);
        assertThat(assetEvent1).isNotEqualTo(assetEvent2);
        assetEvent1.setId(null);
        assertThat(assetEvent1).isNotEqualTo(assetEvent2);
    }
}
