package com.gcd.web.rest;

import com.gcd.GcdApp;
import com.gcd.domain.Asset;
import com.gcd.repository.AssetRepository;
import com.gcd.service.AssetService;
import com.gcd.service.dto.AssetDTO;
import com.gcd.service.mapper.AssetMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AssetResource} REST controller.
 */
@SpringBootTest(classes = GcdApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AssetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOM_TAG = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOM_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Long DEFAULT_STATUS_ID = 1L;
    private static final Long UPDATED_STATUS_ID = 2L;

    private static final LocalDate DEFAULT_PURCHASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PURCHASE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PURCHASE_FROM = "AAAAAAAAAA";
    private static final String UPDATED_PURCHASE_FROM = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PURCHASE_COST = new BigDecimal(1);
    private static final BigDecimal UPDATED_PURCHASE_COST = new BigDecimal(2);

    private static final String DEFAULT_BRAND = "AAAAAAAAAA";
    private static final String UPDATED_BRAND = "BBBBBBBBBB";

    private static final String DEFAULT_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_MODEL = "BBBBBBBBBB";

    private static final String DEFAULT_SERIAL_NO = "AAAAAAAAAA";
    private static final String UPDATED_SERIAL_NO = "BBBBBBBBBB";

    private static final Long DEFAULT_SITE_ID = 1L;
    private static final Long UPDATED_SITE_ID = 2L;

    private static final Long DEFAULT_LOCATION_ID = 1L;
    private static final Long UPDATED_LOCATION_ID = 2L;

    private static final Long DEFAULT_DEPARTMENT_ID = 1L;
    private static final Long UPDATED_DEPARTMENT_ID = 2L;

    private static final Long DEFAULT_CATEGORY_ID = 1L;
    private static final Long UPDATED_CATEGORY_ID = 2L;

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_EOL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EOL_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_ASSET_TYPE_ID = 1L;
    private static final Long UPDATED_ASSET_TYPE_ID = 2L;

    private static final String DEFAULT_SYSTEM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SYSTEM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROCESSOR = "AAAAAAAAAA";
    private static final String UPDATED_PROCESSOR = "BBBBBBBBBB";

    private static final String DEFAULT_HARD_DRIVE = "AAAAAAAAAA";
    private static final String UPDATED_HARD_DRIVE = "BBBBBBBBBB";

    private static final String DEFAULT_MEMORY = "AAAAAAAAAA";
    private static final String UPDATED_MEMORY = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATING_SYSTEM = "AAAAAAAAAA";
    private static final String UPDATED_OPERATING_SYSTEM = "BBBBBBBBBB";

    private static final String DEFAULT_WORK_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_WORK_GROUP = "BBBBBBBBBB";

    private static final String DEFAULT_IP_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_IP_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_BIOS = "AAAAAAAAAA";
    private static final String UPDATED_BIOS = "BBBBBBBBBB";

    private static final String DEFAULT_FIRMWARE = "AAAAAAAAAA";
    private static final String UPDATED_FIRMWARE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final Long DEFAULT_BUSINESS_ID = 1L;
    private static final Long UPDATED_BUSINESS_ID = 2L;

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private AssetMapper assetMapper;

    @Autowired
    private AssetService assetService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetMockMvc;

    private Asset asset;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createEntity(EntityManager em) {
        Asset asset = new Asset()
            .name(DEFAULT_NAME)
            .tag(DEFAULT_TAG)
            .customTag(DEFAULT_CUSTOM_TAG)
            .description(DEFAULT_DESCRIPTION)
            .statusId(DEFAULT_STATUS_ID)
            .purchaseDate(DEFAULT_PURCHASE_DATE)
            .purchaseFrom(DEFAULT_PURCHASE_FROM)
            .purchaseCost(DEFAULT_PURCHASE_COST)
            .brand(DEFAULT_BRAND)
            .model(DEFAULT_MODEL)
            .serialNo(DEFAULT_SERIAL_NO)
            .siteId(DEFAULT_SITE_ID)
            .locationId(DEFAULT_LOCATION_ID)
            .departmentId(DEFAULT_DEPARTMENT_ID)
            .categoryId(DEFAULT_CATEGORY_ID)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .eolDate(DEFAULT_EOL_DATE)
            .assetTypeId(DEFAULT_ASSET_TYPE_ID)
            .systemName(DEFAULT_SYSTEM_NAME)
            .processor(DEFAULT_PROCESSOR)
            .hardDrive(DEFAULT_HARD_DRIVE)
            .memory(DEFAULT_MEMORY)
            .operatingSystem(DEFAULT_OPERATING_SYSTEM)
            .workGroup(DEFAULT_WORK_GROUP)
            .ipAddress(DEFAULT_IP_ADDRESS)
            .bios(DEFAULT_BIOS)
            .firmware(DEFAULT_FIRMWARE)
            .comments(DEFAULT_COMMENTS)
            .businessId(DEFAULT_BUSINESS_ID);
        return asset;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createUpdatedEntity(EntityManager em) {
        Asset asset = new Asset()
            .name(UPDATED_NAME)
            .tag(UPDATED_TAG)
            .customTag(UPDATED_CUSTOM_TAG)
            .description(UPDATED_DESCRIPTION)
            .statusId(UPDATED_STATUS_ID)
            .purchaseDate(UPDATED_PURCHASE_DATE)
            .purchaseFrom(UPDATED_PURCHASE_FROM)
            .purchaseCost(UPDATED_PURCHASE_COST)
            .brand(UPDATED_BRAND)
            .model(UPDATED_MODEL)
            .serialNo(UPDATED_SERIAL_NO)
            .siteId(UPDATED_SITE_ID)
            .locationId(UPDATED_LOCATION_ID)
            .departmentId(UPDATED_DEPARTMENT_ID)
            .categoryId(UPDATED_CATEGORY_ID)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .eolDate(UPDATED_EOL_DATE)
            .assetTypeId(UPDATED_ASSET_TYPE_ID)
            .systemName(UPDATED_SYSTEM_NAME)
            .processor(UPDATED_PROCESSOR)
            .hardDrive(UPDATED_HARD_DRIVE)
            .memory(UPDATED_MEMORY)
            .operatingSystem(UPDATED_OPERATING_SYSTEM)
            .workGroup(UPDATED_WORK_GROUP)
            .ipAddress(UPDATED_IP_ADDRESS)
            .bios(UPDATED_BIOS)
            .firmware(UPDATED_FIRMWARE)
            .comments(UPDATED_COMMENTS)
            .businessId(UPDATED_BUSINESS_ID);
        return asset;
    }

    @BeforeEach
    public void initTest() {
        asset = createEntity(em);
    }

    @Test
    @Transactional
    public void createAsset() throws Exception {
        int databaseSizeBeforeCreate = assetRepository.findAll().size();
        // Create the Asset
        AssetDTO assetDTO = assetMapper.toDto(asset);
        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isCreated());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate + 1);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAsset.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testAsset.getCustomTag()).isEqualTo(DEFAULT_CUSTOM_TAG);
        assertThat(testAsset.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAsset.getStatusId()).isEqualTo(DEFAULT_STATUS_ID);
        assertThat(testAsset.getPurchaseDate()).isEqualTo(DEFAULT_PURCHASE_DATE);
        assertThat(testAsset.getPurchaseFrom()).isEqualTo(DEFAULT_PURCHASE_FROM);
        assertThat(testAsset.getPurchaseCost()).isEqualTo(DEFAULT_PURCHASE_COST);
        assertThat(testAsset.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testAsset.getModel()).isEqualTo(DEFAULT_MODEL);
        assertThat(testAsset.getSerialNo()).isEqualTo(DEFAULT_SERIAL_NO);
        assertThat(testAsset.getSiteId()).isEqualTo(DEFAULT_SITE_ID);
        assertThat(testAsset.getLocationId()).isEqualTo(DEFAULT_LOCATION_ID);
        assertThat(testAsset.getDepartmentId()).isEqualTo(DEFAULT_DEPARTMENT_ID);
        assertThat(testAsset.getCategoryId()).isEqualTo(DEFAULT_CATEGORY_ID);
        assertThat(testAsset.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testAsset.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testAsset.getEolDate()).isEqualTo(DEFAULT_EOL_DATE);
        assertThat(testAsset.getAssetTypeId()).isEqualTo(DEFAULT_ASSET_TYPE_ID);
        assertThat(testAsset.getSystemName()).isEqualTo(DEFAULT_SYSTEM_NAME);
        assertThat(testAsset.getProcessor()).isEqualTo(DEFAULT_PROCESSOR);
        assertThat(testAsset.getHardDrive()).isEqualTo(DEFAULT_HARD_DRIVE);
        assertThat(testAsset.getMemory()).isEqualTo(DEFAULT_MEMORY);
        assertThat(testAsset.getOperatingSystem()).isEqualTo(DEFAULT_OPERATING_SYSTEM);
        assertThat(testAsset.getWorkGroup()).isEqualTo(DEFAULT_WORK_GROUP);
        assertThat(testAsset.getIpAddress()).isEqualTo(DEFAULT_IP_ADDRESS);
        assertThat(testAsset.getBios()).isEqualTo(DEFAULT_BIOS);
        assertThat(testAsset.getFirmware()).isEqualTo(DEFAULT_FIRMWARE);
        assertThat(testAsset.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAsset.getBusinessId()).isEqualTo(DEFAULT_BUSINESS_ID);
    }

    @Test
    @Transactional
    public void createAssetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assetRepository.findAll().size();

        // Create the Asset with an existing ID
        asset.setId(1L);
        AssetDTO assetDTO = assetMapper.toDto(asset);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = assetRepository.findAll().size();
        // set the field null
        asset.setName(null);

        // Create the Asset, which fails.
        AssetDTO assetDTO = assetMapper.toDto(asset);


        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isBadRequest());

        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = assetRepository.findAll().size();
        // set the field null
        asset.setStatusId(null);

        // Create the Asset, which fails.
        AssetDTO assetDTO = assetMapper.toDto(asset);


        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isBadRequest());

        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPurchaseDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = assetRepository.findAll().size();
        // set the field null
        asset.setPurchaseDate(null);

        // Create the Asset, which fails.
        AssetDTO assetDTO = assetMapper.toDto(asset);


        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isBadRequest());

        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssets() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get all the assetList
        restAssetMockMvc.perform(get("/api/assets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(asset.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG)))
            .andExpect(jsonPath("$.[*].customTag").value(hasItem(DEFAULT_CUSTOM_TAG)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].statusId").value(hasItem(DEFAULT_STATUS_ID.intValue())))
            .andExpect(jsonPath("$.[*].purchaseDate").value(hasItem(DEFAULT_PURCHASE_DATE.toString())))
            .andExpect(jsonPath("$.[*].purchaseFrom").value(hasItem(DEFAULT_PURCHASE_FROM)))
            .andExpect(jsonPath("$.[*].purchaseCost").value(hasItem(DEFAULT_PURCHASE_COST.intValue())))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL)))
            .andExpect(jsonPath("$.[*].serialNo").value(hasItem(DEFAULT_SERIAL_NO)))
            .andExpect(jsonPath("$.[*].siteId").value(hasItem(DEFAULT_SITE_ID.intValue())))
            .andExpect(jsonPath("$.[*].locationId").value(hasItem(DEFAULT_LOCATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].departmentId").value(hasItem(DEFAULT_DEPARTMENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID.intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].eolDate").value(hasItem(DEFAULT_EOL_DATE.toString())))
            .andExpect(jsonPath("$.[*].assetTypeId").value(hasItem(DEFAULT_ASSET_TYPE_ID.intValue())))
            .andExpect(jsonPath("$.[*].systemName").value(hasItem(DEFAULT_SYSTEM_NAME)))
            .andExpect(jsonPath("$.[*].processor").value(hasItem(DEFAULT_PROCESSOR)))
            .andExpect(jsonPath("$.[*].hardDrive").value(hasItem(DEFAULT_HARD_DRIVE)))
            .andExpect(jsonPath("$.[*].memory").value(hasItem(DEFAULT_MEMORY)))
            .andExpect(jsonPath("$.[*].operatingSystem").value(hasItem(DEFAULT_OPERATING_SYSTEM)))
            .andExpect(jsonPath("$.[*].workGroup").value(hasItem(DEFAULT_WORK_GROUP)))
            .andExpect(jsonPath("$.[*].ipAddress").value(hasItem(DEFAULT_IP_ADDRESS)))
            .andExpect(jsonPath("$.[*].bios").value(hasItem(DEFAULT_BIOS)))
            .andExpect(jsonPath("$.[*].firmware").value(hasItem(DEFAULT_FIRMWARE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].businessId").value(hasItem(DEFAULT_BUSINESS_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get the asset
        restAssetMockMvc.perform(get("/api/assets/{id}", asset.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(asset.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG))
            .andExpect(jsonPath("$.customTag").value(DEFAULT_CUSTOM_TAG))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.statusId").value(DEFAULT_STATUS_ID.intValue()))
            .andExpect(jsonPath("$.purchaseDate").value(DEFAULT_PURCHASE_DATE.toString()))
            .andExpect(jsonPath("$.purchaseFrom").value(DEFAULT_PURCHASE_FROM))
            .andExpect(jsonPath("$.purchaseCost").value(DEFAULT_PURCHASE_COST.intValue()))
            .andExpect(jsonPath("$.brand").value(DEFAULT_BRAND))
            .andExpect(jsonPath("$.model").value(DEFAULT_MODEL))
            .andExpect(jsonPath("$.serialNo").value(DEFAULT_SERIAL_NO))
            .andExpect(jsonPath("$.siteId").value(DEFAULT_SITE_ID.intValue()))
            .andExpect(jsonPath("$.locationId").value(DEFAULT_LOCATION_ID.intValue()))
            .andExpect(jsonPath("$.departmentId").value(DEFAULT_DEPARTMENT_ID.intValue()))
            .andExpect(jsonPath("$.categoryId").value(DEFAULT_CATEGORY_ID.intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.eolDate").value(DEFAULT_EOL_DATE.toString()))
            .andExpect(jsonPath("$.assetTypeId").value(DEFAULT_ASSET_TYPE_ID.intValue()))
            .andExpect(jsonPath("$.systemName").value(DEFAULT_SYSTEM_NAME))
            .andExpect(jsonPath("$.processor").value(DEFAULT_PROCESSOR))
            .andExpect(jsonPath("$.hardDrive").value(DEFAULT_HARD_DRIVE))
            .andExpect(jsonPath("$.memory").value(DEFAULT_MEMORY))
            .andExpect(jsonPath("$.operatingSystem").value(DEFAULT_OPERATING_SYSTEM))
            .andExpect(jsonPath("$.workGroup").value(DEFAULT_WORK_GROUP))
            .andExpect(jsonPath("$.ipAddress").value(DEFAULT_IP_ADDRESS))
            .andExpect(jsonPath("$.bios").value(DEFAULT_BIOS))
            .andExpect(jsonPath("$.firmware").value(DEFAULT_FIRMWARE))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.businessId").value(DEFAULT_BUSINESS_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAsset() throws Exception {
        // Get the asset
        restAssetMockMvc.perform(get("/api/assets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Update the asset
        Asset updatedAsset = assetRepository.findById(asset.getId()).get();
        // Disconnect from session so that the updates on updatedAsset are not directly saved in db
        em.detach(updatedAsset);
        updatedAsset
            .name(UPDATED_NAME)
            .tag(UPDATED_TAG)
            .customTag(UPDATED_CUSTOM_TAG)
            .description(UPDATED_DESCRIPTION)
            .statusId(UPDATED_STATUS_ID)
            .purchaseDate(UPDATED_PURCHASE_DATE)
            .purchaseFrom(UPDATED_PURCHASE_FROM)
            .purchaseCost(UPDATED_PURCHASE_COST)
            .brand(UPDATED_BRAND)
            .model(UPDATED_MODEL)
            .serialNo(UPDATED_SERIAL_NO)
            .siteId(UPDATED_SITE_ID)
            .locationId(UPDATED_LOCATION_ID)
            .departmentId(UPDATED_DEPARTMENT_ID)
            .categoryId(UPDATED_CATEGORY_ID)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .eolDate(UPDATED_EOL_DATE)
            .assetTypeId(UPDATED_ASSET_TYPE_ID)
            .systemName(UPDATED_SYSTEM_NAME)
            .processor(UPDATED_PROCESSOR)
            .hardDrive(UPDATED_HARD_DRIVE)
            .memory(UPDATED_MEMORY)
            .operatingSystem(UPDATED_OPERATING_SYSTEM)
            .workGroup(UPDATED_WORK_GROUP)
            .ipAddress(UPDATED_IP_ADDRESS)
            .bios(UPDATED_BIOS)
            .firmware(UPDATED_FIRMWARE)
            .comments(UPDATED_COMMENTS)
            .businessId(UPDATED_BUSINESS_ID);
        AssetDTO assetDTO = assetMapper.toDto(updatedAsset);

        restAssetMockMvc.perform(put("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isOk());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAsset.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testAsset.getCustomTag()).isEqualTo(UPDATED_CUSTOM_TAG);
        assertThat(testAsset.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAsset.getStatusId()).isEqualTo(UPDATED_STATUS_ID);
        assertThat(testAsset.getPurchaseDate()).isEqualTo(UPDATED_PURCHASE_DATE);
        assertThat(testAsset.getPurchaseFrom()).isEqualTo(UPDATED_PURCHASE_FROM);
        assertThat(testAsset.getPurchaseCost()).isEqualTo(UPDATED_PURCHASE_COST);
        assertThat(testAsset.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testAsset.getModel()).isEqualTo(UPDATED_MODEL);
        assertThat(testAsset.getSerialNo()).isEqualTo(UPDATED_SERIAL_NO);
        assertThat(testAsset.getSiteId()).isEqualTo(UPDATED_SITE_ID);
        assertThat(testAsset.getLocationId()).isEqualTo(UPDATED_LOCATION_ID);
        assertThat(testAsset.getDepartmentId()).isEqualTo(UPDATED_DEPARTMENT_ID);
        assertThat(testAsset.getCategoryId()).isEqualTo(UPDATED_CATEGORY_ID);
        assertThat(testAsset.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testAsset.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testAsset.getEolDate()).isEqualTo(UPDATED_EOL_DATE);
        assertThat(testAsset.getAssetTypeId()).isEqualTo(UPDATED_ASSET_TYPE_ID);
        assertThat(testAsset.getSystemName()).isEqualTo(UPDATED_SYSTEM_NAME);
        assertThat(testAsset.getProcessor()).isEqualTo(UPDATED_PROCESSOR);
        assertThat(testAsset.getHardDrive()).isEqualTo(UPDATED_HARD_DRIVE);
        assertThat(testAsset.getMemory()).isEqualTo(UPDATED_MEMORY);
        assertThat(testAsset.getOperatingSystem()).isEqualTo(UPDATED_OPERATING_SYSTEM);
        assertThat(testAsset.getWorkGroup()).isEqualTo(UPDATED_WORK_GROUP);
        assertThat(testAsset.getIpAddress()).isEqualTo(UPDATED_IP_ADDRESS);
        assertThat(testAsset.getBios()).isEqualTo(UPDATED_BIOS);
        assertThat(testAsset.getFirmware()).isEqualTo(UPDATED_FIRMWARE);
        assertThat(testAsset.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAsset.getBusinessId()).isEqualTo(UPDATED_BUSINESS_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Create the Asset
        AssetDTO assetDTO = assetMapper.toDto(asset);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetMockMvc.perform(put("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeDelete = assetRepository.findAll().size();

        // Delete the asset
        restAssetMockMvc.perform(delete("/api/assets/{id}", asset.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
