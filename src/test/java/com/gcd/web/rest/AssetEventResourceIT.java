package com.gcd.web.rest;

import com.gcd.GcdApp;
import com.gcd.domain.AssetEvent;
import com.gcd.repository.AssetEventRepository;
import com.gcd.service.AssetEventService;
import com.gcd.service.dto.AssetEventDTO;
import com.gcd.service.mapper.AssetEventMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AssetEventResource} REST controller.
 */
@SpringBootTest(classes = GcdApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AssetEventResourceIT {

    private static final Long DEFAULT_STATUS_ID = 1L;
    private static final Long UPDATED_STATUS_ID = 2L;

    private static final Integer DEFAULT_CHECK_OUT_TO = 1;
    private static final Integer UPDATED_CHECK_OUT_TO = 2;

    private static final Long DEFAULT_ASSIGNED_TO = 1L;
    private static final Long UPDATED_ASSIGNED_TO = 2L;

    private static final LocalDate DEFAULT_CHECK_OUT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CHECK_OUT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_CHECK_IN_FROM = 1;
    private static final Integer UPDATED_CHECK_IN_FROM = 2;

    private static final Long DEFAULT_RETURNED_FROM = 1L;
    private static final Long UPDATED_RETURNED_FROM = 2L;

    private static final LocalDate DEFAULT_CHECK_IN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CHECK_IN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LEASE_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LEASE_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_LEASE_FROM_CUSTOMER_ID = 1L;
    private static final Long UPDATED_LEASE_FROM_CUSTOMER_ID = 2L;

    private static final LocalDate DEFAULT_LEASE_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LEASE_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LEASE_RETURN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LEASE_RETURN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LOST_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LOST_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FOUND_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FOUND_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_REPAIR_SCHEDULE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REPAIR_SCHEDULE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REPAIR_ASSIGNED_TO = "AAAAAAAAAA";
    private static final String UPDATED_REPAIR_ASSIGNED_TO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REPAIR_COMPLETED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REPAIR_COMPLETED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_REPAIR_COST = new BigDecimal(1);
    private static final BigDecimal UPDATED_REPAIR_COST = new BigDecimal(2);

    private static final LocalDate DEFAULT_BROKEN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BROKEN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DISPOSE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DISPOSE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DISPOSE_TO = "AAAAAAAAAA";
    private static final String UPDATED_DISPOSE_TO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DONATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DONATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DONATE_TO = "AAAAAAAAAA";
    private static final String UPDATED_DONATE_TO = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DONATE_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_DONATE_VALUE = new BigDecimal(2);

    private static final Boolean DEFAULT_DEDUCTIBLE = false;
    private static final Boolean UPDATED_DEDUCTIBLE = true;

    private static final LocalDate DEFAULT_SELL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SELL_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_SELL_TO = "AAAAAAAAAA";
    private static final String UPDATED_SELL_TO = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_SELL_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_SELL_VALUE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_DUE_DATE = false;
    private static final Boolean UPDATED_IS_DUE_DATE = true;

    private static final LocalDate DEFAULT_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_SITE_ID = 1L;
    private static final Long UPDATED_SITE_ID = 2L;

    private static final Long DEFAULT_LOCATION_ID = 1L;
    private static final Long UPDATED_LOCATION_ID = 2L;

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final Long DEFAULT_BUSINESS_ID = 1L;
    private static final Long UPDATED_BUSINESS_ID = 2L;

    @Autowired
    private AssetEventRepository assetEventRepository;

    @Autowired
    private AssetEventMapper assetEventMapper;

    @Autowired
    private AssetEventService assetEventService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetEventMockMvc;

    private AssetEvent assetEvent;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetEvent createEntity(EntityManager em) {
        AssetEvent assetEvent = new AssetEvent()
            .statusId(DEFAULT_STATUS_ID)
            .checkOutTo(DEFAULT_CHECK_OUT_TO)
            .assignedTo(DEFAULT_ASSIGNED_TO)
            .checkOutDate(DEFAULT_CHECK_OUT_DATE)
            .checkInFrom(DEFAULT_CHECK_IN_FROM)
            .returnedFrom(DEFAULT_RETURNED_FROM)
            .checkInDate(DEFAULT_CHECK_IN_DATE)
            .leaseStartDate(DEFAULT_LEASE_START_DATE)
            .leaseFromCustomerId(DEFAULT_LEASE_FROM_CUSTOMER_ID)
            .leaseEndDate(DEFAULT_LEASE_END_DATE)
            .leaseReturnDate(DEFAULT_LEASE_RETURN_DATE)
            .lostDate(DEFAULT_LOST_DATE)
            .foundDate(DEFAULT_FOUND_DATE)
            .repairScheduleDate(DEFAULT_REPAIR_SCHEDULE_DATE)
            .repairAssignedTo(DEFAULT_REPAIR_ASSIGNED_TO)
            .repairCompletedDate(DEFAULT_REPAIR_COMPLETED_DATE)
            .repairCost(DEFAULT_REPAIR_COST)
            .brokenDate(DEFAULT_BROKEN_DATE)
            .disposeDate(DEFAULT_DISPOSE_DATE)
            .disposeTo(DEFAULT_DISPOSE_TO)
            .donateDate(DEFAULT_DONATE_DATE)
            .donateTo(DEFAULT_DONATE_TO)
            .donateValue(DEFAULT_DONATE_VALUE)
            .deductible(DEFAULT_DEDUCTIBLE)
            .sellDate(DEFAULT_SELL_DATE)
            .sellTo(DEFAULT_SELL_TO)
            .sellValue(DEFAULT_SELL_VALUE)
            .isDueDate(DEFAULT_IS_DUE_DATE)
            .dueDate(DEFAULT_DUE_DATE)
            .siteId(DEFAULT_SITE_ID)
            .locationId(DEFAULT_LOCATION_ID)
            .comments(DEFAULT_COMMENTS)
            .businessId(DEFAULT_BUSINESS_ID);
        return assetEvent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetEvent createUpdatedEntity(EntityManager em) {
        AssetEvent assetEvent = new AssetEvent()
            .statusId(UPDATED_STATUS_ID)
            .checkOutTo(UPDATED_CHECK_OUT_TO)
            .assignedTo(UPDATED_ASSIGNED_TO)
            .checkOutDate(UPDATED_CHECK_OUT_DATE)
            .checkInFrom(UPDATED_CHECK_IN_FROM)
            .returnedFrom(UPDATED_RETURNED_FROM)
            .checkInDate(UPDATED_CHECK_IN_DATE)
            .leaseStartDate(UPDATED_LEASE_START_DATE)
            .leaseFromCustomerId(UPDATED_LEASE_FROM_CUSTOMER_ID)
            .leaseEndDate(UPDATED_LEASE_END_DATE)
            .leaseReturnDate(UPDATED_LEASE_RETURN_DATE)
            .lostDate(UPDATED_LOST_DATE)
            .foundDate(UPDATED_FOUND_DATE)
            .repairScheduleDate(UPDATED_REPAIR_SCHEDULE_DATE)
            .repairAssignedTo(UPDATED_REPAIR_ASSIGNED_TO)
            .repairCompletedDate(UPDATED_REPAIR_COMPLETED_DATE)
            .repairCost(UPDATED_REPAIR_COST)
            .brokenDate(UPDATED_BROKEN_DATE)
            .disposeDate(UPDATED_DISPOSE_DATE)
            .disposeTo(UPDATED_DISPOSE_TO)
            .donateDate(UPDATED_DONATE_DATE)
            .donateTo(UPDATED_DONATE_TO)
            .donateValue(UPDATED_DONATE_VALUE)
            .deductible(UPDATED_DEDUCTIBLE)
            .sellDate(UPDATED_SELL_DATE)
            .sellTo(UPDATED_SELL_TO)
            .sellValue(UPDATED_SELL_VALUE)
            .isDueDate(UPDATED_IS_DUE_DATE)
            .dueDate(UPDATED_DUE_DATE)
            .siteId(UPDATED_SITE_ID)
            .locationId(UPDATED_LOCATION_ID)
            .comments(UPDATED_COMMENTS)
            .businessId(UPDATED_BUSINESS_ID);
        return assetEvent;
    }

    @BeforeEach
    public void initTest() {
        assetEvent = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssetEvent() throws Exception {
        int databaseSizeBeforeCreate = assetEventRepository.findAll().size();
        // Create the AssetEvent
        AssetEventDTO assetEventDTO = assetEventMapper.toDto(assetEvent);
        restAssetEventMockMvc.perform(post("/api/asset-events")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetEventDTO)))
            .andExpect(status().isCreated());

        // Validate the AssetEvent in the database
        List<AssetEvent> assetEventList = assetEventRepository.findAll();
        assertThat(assetEventList).hasSize(databaseSizeBeforeCreate + 1);
        AssetEvent testAssetEvent = assetEventList.get(assetEventList.size() - 1);
        assertThat(testAssetEvent.getStatusId()).isEqualTo(DEFAULT_STATUS_ID);
        assertThat(testAssetEvent.getCheckOutTo()).isEqualTo(DEFAULT_CHECK_OUT_TO);
        assertThat(testAssetEvent.getAssignedTo()).isEqualTo(DEFAULT_ASSIGNED_TO);
        assertThat(testAssetEvent.getCheckOutDate()).isEqualTo(DEFAULT_CHECK_OUT_DATE);
        assertThat(testAssetEvent.getCheckInFrom()).isEqualTo(DEFAULT_CHECK_IN_FROM);
        assertThat(testAssetEvent.getReturnedFrom()).isEqualTo(DEFAULT_RETURNED_FROM);
        assertThat(testAssetEvent.getCheckInDate()).isEqualTo(DEFAULT_CHECK_IN_DATE);
        assertThat(testAssetEvent.getLeaseStartDate()).isEqualTo(DEFAULT_LEASE_START_DATE);
        assertThat(testAssetEvent.getLeaseFromCustomerId()).isEqualTo(DEFAULT_LEASE_FROM_CUSTOMER_ID);
        assertThat(testAssetEvent.getLeaseEndDate()).isEqualTo(DEFAULT_LEASE_END_DATE);
        assertThat(testAssetEvent.getLeaseReturnDate()).isEqualTo(DEFAULT_LEASE_RETURN_DATE);
        assertThat(testAssetEvent.getLostDate()).isEqualTo(DEFAULT_LOST_DATE);
        assertThat(testAssetEvent.getFoundDate()).isEqualTo(DEFAULT_FOUND_DATE);
        assertThat(testAssetEvent.getRepairScheduleDate()).isEqualTo(DEFAULT_REPAIR_SCHEDULE_DATE);
        assertThat(testAssetEvent.getRepairAssignedTo()).isEqualTo(DEFAULT_REPAIR_ASSIGNED_TO);
        assertThat(testAssetEvent.getRepairCompletedDate()).isEqualTo(DEFAULT_REPAIR_COMPLETED_DATE);
        assertThat(testAssetEvent.getRepairCost()).isEqualTo(DEFAULT_REPAIR_COST);
        assertThat(testAssetEvent.getBrokenDate()).isEqualTo(DEFAULT_BROKEN_DATE);
        assertThat(testAssetEvent.getDisposeDate()).isEqualTo(DEFAULT_DISPOSE_DATE);
        assertThat(testAssetEvent.getDisposeTo()).isEqualTo(DEFAULT_DISPOSE_TO);
        assertThat(testAssetEvent.getDonateDate()).isEqualTo(DEFAULT_DONATE_DATE);
        assertThat(testAssetEvent.getDonateTo()).isEqualTo(DEFAULT_DONATE_TO);
        assertThat(testAssetEvent.getDonateValue()).isEqualTo(DEFAULT_DONATE_VALUE);
        assertThat(testAssetEvent.isDeductible()).isEqualTo(DEFAULT_DEDUCTIBLE);
        assertThat(testAssetEvent.getSellDate()).isEqualTo(DEFAULT_SELL_DATE);
        assertThat(testAssetEvent.getSellTo()).isEqualTo(DEFAULT_SELL_TO);
        assertThat(testAssetEvent.getSellValue()).isEqualTo(DEFAULT_SELL_VALUE);
        assertThat(testAssetEvent.isIsDueDate()).isEqualTo(DEFAULT_IS_DUE_DATE);
        assertThat(testAssetEvent.getDueDate()).isEqualTo(DEFAULT_DUE_DATE);
        assertThat(testAssetEvent.getSiteId()).isEqualTo(DEFAULT_SITE_ID);
        assertThat(testAssetEvent.getLocationId()).isEqualTo(DEFAULT_LOCATION_ID);
        assertThat(testAssetEvent.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAssetEvent.getBusinessId()).isEqualTo(DEFAULT_BUSINESS_ID);
    }

    @Test
    @Transactional
    public void createAssetEventWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assetEventRepository.findAll().size();

        // Create the AssetEvent with an existing ID
        assetEvent.setId(1L);
        AssetEventDTO assetEventDTO = assetEventMapper.toDto(assetEvent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetEventMockMvc.perform(post("/api/asset-events")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetEventDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AssetEvent in the database
        List<AssetEvent> assetEventList = assetEventRepository.findAll();
        assertThat(assetEventList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAssetEvents() throws Exception {
        // Initialize the database
        assetEventRepository.saveAndFlush(assetEvent);

        // Get all the assetEventList
        restAssetEventMockMvc.perform(get("/api/asset-events?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assetEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusId").value(hasItem(DEFAULT_STATUS_ID.intValue())))
            .andExpect(jsonPath("$.[*].checkOutTo").value(hasItem(DEFAULT_CHECK_OUT_TO)))
            .andExpect(jsonPath("$.[*].assignedTo").value(hasItem(DEFAULT_ASSIGNED_TO.intValue())))
            .andExpect(jsonPath("$.[*].checkOutDate").value(hasItem(DEFAULT_CHECK_OUT_DATE.toString())))
            .andExpect(jsonPath("$.[*].checkInFrom").value(hasItem(DEFAULT_CHECK_IN_FROM)))
            .andExpect(jsonPath("$.[*].returnedFrom").value(hasItem(DEFAULT_RETURNED_FROM.intValue())))
            .andExpect(jsonPath("$.[*].checkInDate").value(hasItem(DEFAULT_CHECK_IN_DATE.toString())))
            .andExpect(jsonPath("$.[*].leaseStartDate").value(hasItem(DEFAULT_LEASE_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].leaseFromCustomerId").value(hasItem(DEFAULT_LEASE_FROM_CUSTOMER_ID.intValue())))
            .andExpect(jsonPath("$.[*].leaseEndDate").value(hasItem(DEFAULT_LEASE_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].leaseReturnDate").value(hasItem(DEFAULT_LEASE_RETURN_DATE.toString())))
            .andExpect(jsonPath("$.[*].lostDate").value(hasItem(DEFAULT_LOST_DATE.toString())))
            .andExpect(jsonPath("$.[*].foundDate").value(hasItem(DEFAULT_FOUND_DATE.toString())))
            .andExpect(jsonPath("$.[*].repairScheduleDate").value(hasItem(DEFAULT_REPAIR_SCHEDULE_DATE.toString())))
            .andExpect(jsonPath("$.[*].repairAssignedTo").value(hasItem(DEFAULT_REPAIR_ASSIGNED_TO)))
            .andExpect(jsonPath("$.[*].repairCompletedDate").value(hasItem(DEFAULT_REPAIR_COMPLETED_DATE.toString())))
            .andExpect(jsonPath("$.[*].repairCost").value(hasItem(DEFAULT_REPAIR_COST.intValue())))
            .andExpect(jsonPath("$.[*].brokenDate").value(hasItem(DEFAULT_BROKEN_DATE.toString())))
            .andExpect(jsonPath("$.[*].disposeDate").value(hasItem(DEFAULT_DISPOSE_DATE.toString())))
            .andExpect(jsonPath("$.[*].disposeTo").value(hasItem(DEFAULT_DISPOSE_TO)))
            .andExpect(jsonPath("$.[*].donateDate").value(hasItem(DEFAULT_DONATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].donateTo").value(hasItem(DEFAULT_DONATE_TO)))
            .andExpect(jsonPath("$.[*].donateValue").value(hasItem(DEFAULT_DONATE_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].deductible").value(hasItem(DEFAULT_DEDUCTIBLE.booleanValue())))
            .andExpect(jsonPath("$.[*].sellDate").value(hasItem(DEFAULT_SELL_DATE.toString())))
            .andExpect(jsonPath("$.[*].sellTo").value(hasItem(DEFAULT_SELL_TO)))
            .andExpect(jsonPath("$.[*].sellValue").value(hasItem(DEFAULT_SELL_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].isDueDate").value(hasItem(DEFAULT_IS_DUE_DATE.booleanValue())))
            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].siteId").value(hasItem(DEFAULT_SITE_ID.intValue())))
            .andExpect(jsonPath("$.[*].locationId").value(hasItem(DEFAULT_LOCATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].businessId").value(hasItem(DEFAULT_BUSINESS_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getAssetEvent() throws Exception {
        // Initialize the database
        assetEventRepository.saveAndFlush(assetEvent);

        // Get the assetEvent
        restAssetEventMockMvc.perform(get("/api/asset-events/{id}", assetEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assetEvent.getId().intValue()))
            .andExpect(jsonPath("$.statusId").value(DEFAULT_STATUS_ID.intValue()))
            .andExpect(jsonPath("$.checkOutTo").value(DEFAULT_CHECK_OUT_TO))
            .andExpect(jsonPath("$.assignedTo").value(DEFAULT_ASSIGNED_TO.intValue()))
            .andExpect(jsonPath("$.checkOutDate").value(DEFAULT_CHECK_OUT_DATE.toString()))
            .andExpect(jsonPath("$.checkInFrom").value(DEFAULT_CHECK_IN_FROM))
            .andExpect(jsonPath("$.returnedFrom").value(DEFAULT_RETURNED_FROM.intValue()))
            .andExpect(jsonPath("$.checkInDate").value(DEFAULT_CHECK_IN_DATE.toString()))
            .andExpect(jsonPath("$.leaseStartDate").value(DEFAULT_LEASE_START_DATE.toString()))
            .andExpect(jsonPath("$.leaseFromCustomerId").value(DEFAULT_LEASE_FROM_CUSTOMER_ID.intValue()))
            .andExpect(jsonPath("$.leaseEndDate").value(DEFAULT_LEASE_END_DATE.toString()))
            .andExpect(jsonPath("$.leaseReturnDate").value(DEFAULT_LEASE_RETURN_DATE.toString()))
            .andExpect(jsonPath("$.lostDate").value(DEFAULT_LOST_DATE.toString()))
            .andExpect(jsonPath("$.foundDate").value(DEFAULT_FOUND_DATE.toString()))
            .andExpect(jsonPath("$.repairScheduleDate").value(DEFAULT_REPAIR_SCHEDULE_DATE.toString()))
            .andExpect(jsonPath("$.repairAssignedTo").value(DEFAULT_REPAIR_ASSIGNED_TO))
            .andExpect(jsonPath("$.repairCompletedDate").value(DEFAULT_REPAIR_COMPLETED_DATE.toString()))
            .andExpect(jsonPath("$.repairCost").value(DEFAULT_REPAIR_COST.intValue()))
            .andExpect(jsonPath("$.brokenDate").value(DEFAULT_BROKEN_DATE.toString()))
            .andExpect(jsonPath("$.disposeDate").value(DEFAULT_DISPOSE_DATE.toString()))
            .andExpect(jsonPath("$.disposeTo").value(DEFAULT_DISPOSE_TO))
            .andExpect(jsonPath("$.donateDate").value(DEFAULT_DONATE_DATE.toString()))
            .andExpect(jsonPath("$.donateTo").value(DEFAULT_DONATE_TO))
            .andExpect(jsonPath("$.donateValue").value(DEFAULT_DONATE_VALUE.intValue()))
            .andExpect(jsonPath("$.deductible").value(DEFAULT_DEDUCTIBLE.booleanValue()))
            .andExpect(jsonPath("$.sellDate").value(DEFAULT_SELL_DATE.toString()))
            .andExpect(jsonPath("$.sellTo").value(DEFAULT_SELL_TO))
            .andExpect(jsonPath("$.sellValue").value(DEFAULT_SELL_VALUE.intValue()))
            .andExpect(jsonPath("$.isDueDate").value(DEFAULT_IS_DUE_DATE.booleanValue()))
            .andExpect(jsonPath("$.dueDate").value(DEFAULT_DUE_DATE.toString()))
            .andExpect(jsonPath("$.siteId").value(DEFAULT_SITE_ID.intValue()))
            .andExpect(jsonPath("$.locationId").value(DEFAULT_LOCATION_ID.intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.businessId").value(DEFAULT_BUSINESS_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAssetEvent() throws Exception {
        // Get the assetEvent
        restAssetEventMockMvc.perform(get("/api/asset-events/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssetEvent() throws Exception {
        // Initialize the database
        assetEventRepository.saveAndFlush(assetEvent);

        int databaseSizeBeforeUpdate = assetEventRepository.findAll().size();

        // Update the assetEvent
        AssetEvent updatedAssetEvent = assetEventRepository.findById(assetEvent.getId()).get();
        // Disconnect from session so that the updates on updatedAssetEvent are not directly saved in db
        em.detach(updatedAssetEvent);
        updatedAssetEvent
            .statusId(UPDATED_STATUS_ID)
            .checkOutTo(UPDATED_CHECK_OUT_TO)
            .assignedTo(UPDATED_ASSIGNED_TO)
            .checkOutDate(UPDATED_CHECK_OUT_DATE)
            .checkInFrom(UPDATED_CHECK_IN_FROM)
            .returnedFrom(UPDATED_RETURNED_FROM)
            .checkInDate(UPDATED_CHECK_IN_DATE)
            .leaseStartDate(UPDATED_LEASE_START_DATE)
            .leaseFromCustomerId(UPDATED_LEASE_FROM_CUSTOMER_ID)
            .leaseEndDate(UPDATED_LEASE_END_DATE)
            .leaseReturnDate(UPDATED_LEASE_RETURN_DATE)
            .lostDate(UPDATED_LOST_DATE)
            .foundDate(UPDATED_FOUND_DATE)
            .repairScheduleDate(UPDATED_REPAIR_SCHEDULE_DATE)
            .repairAssignedTo(UPDATED_REPAIR_ASSIGNED_TO)
            .repairCompletedDate(UPDATED_REPAIR_COMPLETED_DATE)
            .repairCost(UPDATED_REPAIR_COST)
            .brokenDate(UPDATED_BROKEN_DATE)
            .disposeDate(UPDATED_DISPOSE_DATE)
            .disposeTo(UPDATED_DISPOSE_TO)
            .donateDate(UPDATED_DONATE_DATE)
            .donateTo(UPDATED_DONATE_TO)
            .donateValue(UPDATED_DONATE_VALUE)
            .deductible(UPDATED_DEDUCTIBLE)
            .sellDate(UPDATED_SELL_DATE)
            .sellTo(UPDATED_SELL_TO)
            .sellValue(UPDATED_SELL_VALUE)
            .isDueDate(UPDATED_IS_DUE_DATE)
            .dueDate(UPDATED_DUE_DATE)
            .siteId(UPDATED_SITE_ID)
            .locationId(UPDATED_LOCATION_ID)
            .comments(UPDATED_COMMENTS)
            .businessId(UPDATED_BUSINESS_ID);
        AssetEventDTO assetEventDTO = assetEventMapper.toDto(updatedAssetEvent);

        restAssetEventMockMvc.perform(put("/api/asset-events")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetEventDTO)))
            .andExpect(status().isOk());

        // Validate the AssetEvent in the database
        List<AssetEvent> assetEventList = assetEventRepository.findAll();
        assertThat(assetEventList).hasSize(databaseSizeBeforeUpdate);
        AssetEvent testAssetEvent = assetEventList.get(assetEventList.size() - 1);
        assertThat(testAssetEvent.getStatusId()).isEqualTo(UPDATED_STATUS_ID);
        assertThat(testAssetEvent.getCheckOutTo()).isEqualTo(UPDATED_CHECK_OUT_TO);
        assertThat(testAssetEvent.getAssignedTo()).isEqualTo(UPDATED_ASSIGNED_TO);
        assertThat(testAssetEvent.getCheckOutDate()).isEqualTo(UPDATED_CHECK_OUT_DATE);
        assertThat(testAssetEvent.getCheckInFrom()).isEqualTo(UPDATED_CHECK_IN_FROM);
        assertThat(testAssetEvent.getReturnedFrom()).isEqualTo(UPDATED_RETURNED_FROM);
        assertThat(testAssetEvent.getCheckInDate()).isEqualTo(UPDATED_CHECK_IN_DATE);
        assertThat(testAssetEvent.getLeaseStartDate()).isEqualTo(UPDATED_LEASE_START_DATE);
        assertThat(testAssetEvent.getLeaseFromCustomerId()).isEqualTo(UPDATED_LEASE_FROM_CUSTOMER_ID);
        assertThat(testAssetEvent.getLeaseEndDate()).isEqualTo(UPDATED_LEASE_END_DATE);
        assertThat(testAssetEvent.getLeaseReturnDate()).isEqualTo(UPDATED_LEASE_RETURN_DATE);
        assertThat(testAssetEvent.getLostDate()).isEqualTo(UPDATED_LOST_DATE);
        assertThat(testAssetEvent.getFoundDate()).isEqualTo(UPDATED_FOUND_DATE);
        assertThat(testAssetEvent.getRepairScheduleDate()).isEqualTo(UPDATED_REPAIR_SCHEDULE_DATE);
        assertThat(testAssetEvent.getRepairAssignedTo()).isEqualTo(UPDATED_REPAIR_ASSIGNED_TO);
        assertThat(testAssetEvent.getRepairCompletedDate()).isEqualTo(UPDATED_REPAIR_COMPLETED_DATE);
        assertThat(testAssetEvent.getRepairCost()).isEqualTo(UPDATED_REPAIR_COST);
        assertThat(testAssetEvent.getBrokenDate()).isEqualTo(UPDATED_BROKEN_DATE);
        assertThat(testAssetEvent.getDisposeDate()).isEqualTo(UPDATED_DISPOSE_DATE);
        assertThat(testAssetEvent.getDisposeTo()).isEqualTo(UPDATED_DISPOSE_TO);
        assertThat(testAssetEvent.getDonateDate()).isEqualTo(UPDATED_DONATE_DATE);
        assertThat(testAssetEvent.getDonateTo()).isEqualTo(UPDATED_DONATE_TO);
        assertThat(testAssetEvent.getDonateValue()).isEqualTo(UPDATED_DONATE_VALUE);
        assertThat(testAssetEvent.isDeductible()).isEqualTo(UPDATED_DEDUCTIBLE);
        assertThat(testAssetEvent.getSellDate()).isEqualTo(UPDATED_SELL_DATE);
        assertThat(testAssetEvent.getSellTo()).isEqualTo(UPDATED_SELL_TO);
        assertThat(testAssetEvent.getSellValue()).isEqualTo(UPDATED_SELL_VALUE);
        assertThat(testAssetEvent.isIsDueDate()).isEqualTo(UPDATED_IS_DUE_DATE);
        assertThat(testAssetEvent.getDueDate()).isEqualTo(UPDATED_DUE_DATE);
        assertThat(testAssetEvent.getSiteId()).isEqualTo(UPDATED_SITE_ID);
        assertThat(testAssetEvent.getLocationId()).isEqualTo(UPDATED_LOCATION_ID);
        assertThat(testAssetEvent.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAssetEvent.getBusinessId()).isEqualTo(UPDATED_BUSINESS_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAssetEvent() throws Exception {
        int databaseSizeBeforeUpdate = assetEventRepository.findAll().size();

        // Create the AssetEvent
        AssetEventDTO assetEventDTO = assetEventMapper.toDto(assetEvent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetEventMockMvc.perform(put("/api/asset-events")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assetEventDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AssetEvent in the database
        List<AssetEvent> assetEventList = assetEventRepository.findAll();
        assertThat(assetEventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAssetEvent() throws Exception {
        // Initialize the database
        assetEventRepository.saveAndFlush(assetEvent);

        int databaseSizeBeforeDelete = assetEventRepository.findAll().size();

        // Delete the assetEvent
        restAssetEventMockMvc.perform(delete("/api/asset-events/{id}", assetEvent.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AssetEvent> assetEventList = assetEventRepository.findAll();
        assertThat(assetEventList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
