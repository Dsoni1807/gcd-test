package com.gcd.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AssetEventMapperTest {

    private AssetEventMapper assetEventMapper;

    @BeforeEach
    public void setUp() {
        assetEventMapper = new AssetEventMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(assetEventMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(assetEventMapper.fromId(null)).isNull();
    }
}
