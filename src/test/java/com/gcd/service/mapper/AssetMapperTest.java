package com.gcd.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AssetMapperTest {

    private AssetMapper assetMapper;

    @BeforeEach
    public void setUp() {
        assetMapper = new AssetMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(assetMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(assetMapper.fromId(null)).isNull();
    }
}
