package com.gcd.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.gcd.web.rest.TestUtil;

public class AssetEventDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssetEventDTO.class);
        AssetEventDTO assetEventDTO1 = new AssetEventDTO();
        assetEventDTO1.setId(1L);
        AssetEventDTO assetEventDTO2 = new AssetEventDTO();
        assertThat(assetEventDTO1).isNotEqualTo(assetEventDTO2);
        assetEventDTO2.setId(assetEventDTO1.getId());
        assertThat(assetEventDTO1).isEqualTo(assetEventDTO2);
        assetEventDTO2.setId(2L);
        assertThat(assetEventDTO1).isNotEqualTo(assetEventDTO2);
        assetEventDTO1.setId(null);
        assertThat(assetEventDTO1).isNotEqualTo(assetEventDTO2);
    }
}
